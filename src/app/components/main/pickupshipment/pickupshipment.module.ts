import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadpickupshipmentComponent } from './uploadpickupshipment/uploadpickupshipment.component';
import { PickupshipmentComponent } from './pickupshipment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, ModalModule, AlertModule, BsDatepickerModule, TimepickerModule, TypeaheadModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './pickupshipment.routes'

import { ProgressHttpModule } from 'angular-progress-http';
import { DataService, UploadService, UtilityService, NotificationService } from 'src/app/core/services';
import { PickupShipmentService } from 'src/app/core/api';
import { TranslateModule } from 'src/app/core/pipes';
import { UploadpickupshipmentDetailComponent } from './UploadpickupshipmentDetail/uploadpickupshipment-detail.component';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { uniqueTrackingNumberDirective } from './uniqueTrackingNumber.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProgressHttpModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forChild(mainRoutes),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    ReactiveFormsModule,
    TranslateModule,
    NguiAutoCompleteModule,
    AngularDateTimePickerModule
  ],
  declarations: [
    UploadpickupshipmentComponent,
    PickupshipmentComponent,
    UploadpickupshipmentDetailComponent,
    uniqueTrackingNumberDirective
  ],
  providers : [
    DataService,
    UploadService,
    UtilityService,
    NotificationService,
    PickupShipmentService
  ],
  entryComponents: [UploadpickupshipmentDetailComponent]
})
export class PickupshipmentModule { }
