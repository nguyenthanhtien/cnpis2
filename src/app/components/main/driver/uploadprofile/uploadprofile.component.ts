import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NgForm  } from '@angular/forms';

import { ProgressHttp } from 'angular-progress-http';
import { Response, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';

import { Http } from '@angular/http';
import { saveAs } from 'file-saver';

import { ModalDirective } from 'ngx-bootstrap';
import { UploadService, NotificationService, UtilityService } from 'src/app/core/services';
import { MessageContstants, UrlConstants } from 'src/app/core/common';
import { DriverUpload } from 'src/app/core/models';
import { AppConfig } from '../../../../app.config';
import { TranslatePipe } from '../../../../core/pipes/translate.pipe';

@Component({
  selector: 'app-upload-profile',
  templateUrl: './uploadprofile.component.html',
})
export class UploadprofileComponent implements OnInit {
  @ViewChild('modalEdit') public modalEdit: ModalDirective;
  public formUpload : FormGroup;
  public fileData: File[];
  public processPercent: number;
  public sumaryReport: DriverUpload[]= [];
  public fileDown: any;
  profileLength: number;
  driverDto : any;
  pager: any = {};
  pagedItems: any;
  toUpdates : string[] = [];
  

  constructor(
    private pipeTranslate : TranslatePipe,
    private uploadService: UploadService,
    private processHttp: ProgressHttp,
    private notificationService: NotificationService,
    private utilityService : UtilityService,
    private _http: Http,
    ) {
  }

  
  ngOnInit() {
    
  }
  
  getFiles(fileProfiles: File[]) {
    return this.fileData = fileProfiles;
  }

  showProgressBar(): boolean {
    return this.processPercent != null && this.processPercent > 0;
  }

  checkValidFileType(fileName: string): boolean {
    var filePart = fileName.split('.');
    var fileExtension = filePart[1];
    if (fileExtension == 'csv' || fileExtension == 'xls' || fileExtension == 'xlsx') {
      return true;
    }
    return false;
  }

  postFileToServer() {
    const formData = new FormData();
    formData.append("FileUploaded", this.fileData[0]);
    if (this.checkValidFileType(this.fileData[0].name)) {
      this.processHttp
        .withUploadProgressListener(process => {
          this.processPercent = process.percentage
        })
        .post(AppConfig.BASE_API + UrlConstants.UPLOAD_DRIVER_PROFILE_FILE, formData)
        .map((res: Response) => res.json()).subscribe(
          (result: DriverUpload[]) => {
            this.sumaryReport = result;
            this.fileDown = result;
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-upload-profile-ok',null));
            this.resetProgress();
          }, (err) => {
            var res = err._body.split(",",2);
            if(res[1] = ' 2224"'){
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-upload-profile-file-invalid',null));
              this.resetProgress();
            }else{
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-upload-profile-err',null));
              this.resetProgress();
            }
          });
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-upload-profile-file-invalid',null));
    }

  }

  resetProgress() {
    this.processPercent = 0;
    this.fileData = null;
  }
  
  downloadTemplateProfile() {
    const options = new RequestOptions({ responseType: ResponseContentType.Blob })
    return this._http.get(AppConfig.BASE_API + UrlConstants.DOWNLOAD_TEMPLATE_PROFILE, options)
      .map((response: Response) => <Blob>response.blob()).subscribe(data => {
        const b = new Blob([data], { type: 'text/csv' });
        saveAs(b, UrlConstants.FILENAME_PROFILE_TEMPLATE);
      });
  }

  downloadFailedRecords(){
    let url= this.fileDown.ReportFilePath;
    window.open(url);
  }

}
