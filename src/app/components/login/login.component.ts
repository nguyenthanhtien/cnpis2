import { Component, OnInit } from '@angular/core';
import { AuthService, NotificationService, UtilityService } from 'src/app/core/services';
import { UrlConstants } from '../../core/common';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { AppConfig } from '../../app.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [TranslatePipe]
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  public  returnUrl: string;
  constructor(private authService: AuthService,
    private notifyService: NotificationService,
    private utilityService: UtilityService,
    private pipeTranslate: TranslatePipe,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.isLoggged();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  isLoggged() {
    if (this.authService.isAuthenticated()) {
      this.utilityService.navigate(UrlConstants.MAIN);
    }
  }
  login() {
    if (this.username != null && this.password != null) {
      this.authService.login(this.username, this.password);
      if (this.authService.isAuthenticated()) {
        this.authService.login(this.username, this.password);
        this.notifyService.printSuccessMessage(this.pipeTranslate.transform('msg-login-success', null));
        this.utilityService.navigate(this.returnUrl);
      } else {
        this.notifyService.printErrorMessage(this.pipeTranslate.transform('msg-login-fail', null))
      }
    }
  }

}
