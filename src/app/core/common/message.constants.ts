export class MessageContstants {
    public static SYSTEM_ERROR_MSG = "There was an error connecting to the server";
    public static CONFIRM_DELETE_MSG = "Are you sure you want to delete this record?";
    public static CREATED_OK_MSG = "Add new successfully!";
    public static UPDATED_OK_MSG = "Update successfully!";
    public static DELETED_OK_MSG = "Delete successfully!";
    public static FORBIDDEN = "You do not have permission for access!";
    public static UNAUTHORIZED = "You are not authorized to access!";
    public static APINOTFOUND = "API not found!";
    public static NOT_DATA = "No item found!";
    public static MSG1 = "is required";
    public static MSG2 = "{0} is/are {1} successfully.";
    public static MSG3 = "An error occurred when {0} {1}";
    public static MSG4 = "Wrong format for period of time.";
    public static MSG5 = "Login session has expired, please log in again.";
    public static MSG6 = "{0} already exists.";
    public static MSG8 = "Please select CSV file before uploading.";
    public static MSG9 = "The value is invalid.";
    public static MSG10 = "The file format is invalid.";
    public static DRIVER_PROFILE_UPLOAD_SUCCESS = "Driver Profile is/are uploaded successfully."
    public static DRIVER_PROFILE_UPLOAD_FAIL = "An error occurred when uploading Driver Profile."
    
}