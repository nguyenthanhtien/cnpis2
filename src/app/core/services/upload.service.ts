
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { UtilityService } from './utility.service';
import { ProgressHttp, Progress } from 'angular-progress-http';
import { AppConfig } from '../../app.config';

@Injectable()
export class UploadService {
  public responseData: any;
  
  constructor(private dataService: DataService, private utilityService: UtilityService, private processHttp: ProgressHttp) { }

  postWithFile(url: string, postData: any, files: File[]) {
    let formData: FormData = new FormData();
    formData.append('files', files[0], files[0].name);

    if (postData !== "" && postData !== undefined && postData !== null) {
      for (var property in postData) {
        if (postData.hasOwnProperty(property)) {
          formData.append(property, postData[property]);
        }
      }
    }
    var returnReponse = new Promise((resolve, reject) => {
      this.dataService.postFile(url, formData).subscribe(
        res => {
          this.responseData = res;
          resolve( [this.responseData,]);
        },
        error => this.dataService.handleError(error)
      );
    });
    return returnReponse;
  }
  postFileDataToServer (url : string, postData : any, progressCallback : number){

    var returnResponse = new Promise((resolved, rejected) => {
      this.processHttp
      .withUploadProgressListener(process => {
        progressCallback = process.percentage;
      })
      .post(AppConfig.BASE_API + url, postData)
      .map( res => res.json() ).subscribe( res => {
        this.responseData = res;
          resolved(this.responseData);
      }, err => {
        this.dataService.handleError(err);
      });
    });
    return returnResponse;
    
  }
}
