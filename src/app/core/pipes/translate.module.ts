import { TranslatePipe } from "./translate.pipe";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FacilityPipe } from "./translate.user.pipe";
@NgModule({
    imports:      [ CommonModule ],
    declarations: [ TranslatePipe, FacilityPipe],
    exports:      [ TranslatePipe, FacilityPipe],
  })
  export class TranslateModule { }
  