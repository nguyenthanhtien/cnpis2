import { CanActivate, Router, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import { UtilityService } from "./utility.service";
import * as _ from 'lodash';
import { AppConfig } from "../../app.config";

@Injectable()
export class RoleGuardService implements CanActivate {
    constructor(
        public _authService: AuthService,
        public _router: Router,
        public _utilityService: UtilityService,
        public _activeRoute : ActivatedRoute) {
    }
    canActivate(route: ActivatedRouteSnapshot) {
        if (this._authService.isAuthenticated()) {
            let currentRoles = localStorage.getItem(AppConfig.CURRENT_ROLE).split(',');
            let expertedRoles = route.data.expectedRole;
            if (_.filter(currentRoles, e => _.includes(expertedRoles, e)).length > 0) {
                return true;
            }
            this._router.navigate(['/404'], { skipLocationChange: true });
            return false;


        }
        return false;
    }
}