export class CommonRegexp {
    public static NUMERIC_REGEXP = '^[0-9]*$';
    public static ALPHABETS_REGEXP = "^[a-zA-Z][a-zA-Z ]*$";
    public static ALPHABETS_SPECIALCHAR_REGEXP = "^[a-zA-Z][-a-zA-Z0-9+&@#/%=~_| ]*$";
    public static EMAIL_ADDRESS_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    public static PASSWORD =/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9\d]{6,50}$/
    public static URL_REGEXP = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    public static ALPHA_NUMERIC = '^[0-9a-zA-Z]*$';
    public static NUMERIC_FLOAT_REGEXP = /^(([0-9]{0,1}[0-9]{0,})+(\.\d{1,5})?)$/;
    public static PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z]).+$/;
    public static DATE_FORMAT = /^(19[5-9][0-9]|20[0-4][0-9]|2050)[-/](0?[1-9]|1[0-2])[-/](0?[1-9]|[12][0-9]|3[01])$/;
    public static TIME_FORMAT = /^(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])$/;
    public static DATE_TIME_FORMAT = '(\d{4})/(\d{2})/(\d{2}) (\d{2}):(\d{2}):(\d{2})';
    public static NUMBER_CODE = '^[0,9]{2}';
  }