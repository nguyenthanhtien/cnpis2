import { Injectable } from "@angular/core";
import { RequestOptions, Http } from "@angular/http";
import { AppConfig } from "../../app.config";
@Injectable()
export class AuthService {
    constructor(_httt: Http) {

    }
    public login(username: string, password: string) {
        if (username == 'admin' && password == 'admin') {
            localStorage.setItem(AppConfig.CT_REMOTE_USER,username);
            localStorage.setItem(AppConfig.CURRENT_ROLE, 'SYSTEMADMIN,DISTRICTADMIN,HUBCENTERUSER,ITAPPSUPPORT');
            //this.httpHeaders.set(AppConfig.CT_REMOTE_USER, username);
        }
    }
    public logout() {
        localStorage.removeItem(AppConfig.CT_REMOTE_USER);
    }
    public isAuthenticated(): boolean {
        const tokenUser = localStorage.getItem(AppConfig.CT_REMOTE_USER);
        if (tokenUser != null) {
            return true;
        }
        return false;
    }
    
    public getPermission(username: string): string {
        if (username != null) {
            return localStorage.getItem(AppConfig.CURRENT_ROLE);
        }
        return null;
    }


}