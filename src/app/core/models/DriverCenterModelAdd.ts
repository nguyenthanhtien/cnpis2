
export class DriverCenterAdd {
    public UPSFacilitySLIC: string;
    public UPSFacilityName: string;
    public PostalCode: number;
    public Street : string;
    public CountyId : string;
    public DriverName : string;
    public DefaultDriver: string;
    public DriverId: string;
 }