import { Routes } from '@angular/router';
import { EdimessagelistComponent } from './edimessagelist/edimessagelist.component';
import { EdimessagebatchListComponent } from './edimessagebatch-list/edimessagebatch-list.component';
import { EdimessagebatchDetailComponent } from './edimessagebatch-detail/edimessagebatch-detail.component';
import { ExceptionalertsComponent } from './exceptionalerts/exceptionalerts.component';
import { AuthGuardService } from 'src/app/core/services/guard.service';
import { AUTHGROUP } from 'src/app/core/common';
import { RoleGuardService } from '../../../core/services';

export const mainRoutes: Routes = [
    { path: 'edimessage-list', component: EdimessagelistComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN, AUTHGROUP.IT_APP_SUPPORT] } },

    { path: 'edi-batchlist', component: EdimessagebatchListComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN, AUTHGROUP.IT_APP_SUPPORT] } },

    { path: 'edi-batchdetail', component: EdimessagebatchDetailComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN, AUTHGROUP.IT_APP_SUPPORT] } },

    { path: 'exception-alert', component: ExceptionalertsComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN, AUTHGROUP.IT_APP_SUPPORT] } }

]