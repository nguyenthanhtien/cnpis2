import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from '../../../../node_modules/rxjs';
import { PickupShipmentQueryResult } from 'src/app/core/models/PickupShipment';
import { UrlConstants } from '../common/url.constants';
import { PickupShipment } from '../models/PickupShipment';
import { PickupShipmentUpload } from '../models/PickupShipmentUpload';

@Injectable()
export class PickupShipmentService {
    constructor(public dataService: DataService
    ) { }

    getDetailPickupShipment(id: string): Observable<PickupShipment> {
        return this.dataService.get(UrlConstants.PICKUPSHIPMENT_DETAIL + id);
    }

    getAllPickupShipment(params: any): Observable<PickupShipmentQueryResult> {
        return this.dataService.post(UrlConstants.PICKUPSHIPMENT_SEARCH, params);
    }

    putUpdatePickupShipment(data?: any): any {
        return this.dataService.put(UrlConstants.PICKUPSHIPMENT_UPDATE, data);
    }

    deletePickupShipment(id: string): any {
        return this.dataService.delete(UrlConstants.PICKUPSHIPMENT_DELETE + id, 'ShipmentId', id)
    }

    deleteMultiPickupShipment(params: any): any {
        return this.dataService.deleteMultiple(UrlConstants.PICKUPSHIPMENT_DELETEMULTI, params)
    }

    public CheckShipment(shipment: PickupShipmentUpload) {
        return this.dataService.post(UrlConstants.PICKUPSHIPMENT_VALIDATION, shipment);
    }
}