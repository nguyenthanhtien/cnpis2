import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EdimessagelistComponent } from './edimessagelist/edimessagelist.component';
import { EdimessagebatchListComponent } from './edimessagebatch-list/edimessagebatch-list.component';
import { EdimessagebatchDetailComponent } from './edimessagebatch-detail/edimessagebatch-detail.component';
import { ExceptionalertsComponent } from './exceptionalerts/exceptionalerts.component';
import { FormsModule } from '@angular/forms';
import { PaginationModule, ModalModule, AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './editransmission.routes';
import { ProgressHttpModule } from 'angular-progress-http';
import { TranslateModule } from 'src/app/core/pipes/translate.module';
import { DataService, UploadService, UtilityService, NotificationService } from 'src/app/core/services';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProgressHttpModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forChild(mainRoutes),
    TranslateModule
  ],
  declarations: [
    EdimessagelistComponent, 
    EdimessagebatchListComponent,
    EdimessagebatchDetailComponent, 
    ExceptionalertsComponent, 
  ],
  providers : [ DataService,
    UploadService,
    UtilityService,
    NotificationService,]
})
export class EditransmissionModule { }