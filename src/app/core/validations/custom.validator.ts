import { FormControl, Validators } from '@angular/forms';
import { CommonRegexp } from './commonregexp';

export class CustomValidators extends Validators {


    static validDateControl(control: FormControl) {
        if (control.value && control.value.length > 0) {
            const matches = control.value.match(CommonRegexp.DATE_FORMAT);
            return matches && matches.length ? { invalid_characters: matches } : null;
        } else {
            return null;
        }
    }
    static validTimeControl(control: FormControl) {
        if (control.value && control.value.length > 0) {
            const matches = control.value.match(CommonRegexp.TIME_FORMAT);
            return matches && matches.length ? { invalid_characters: matches } : null;
        } else {
            return null;
        }
    }
    static isNotWhitespace(control: FormControl) {
        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true }
    }
    static isNumberic(control: FormControl) {
        if (control.value && control.value.length > 0) {
            let matches = control.value.match(CommonRegexp.NUMERIC_REGEXP);
            return (matches == null || matches.length == 0) ? { invalid_characters: true } : null;
        } else {
            return null;
        }
    }
    static isAlphaAndNumberic(control: FormControl) {
        if (control.value && control.value.length > 0) {
            const matches = control.value.match(CommonRegexp.ALPHA_NUMERIC);
            return matches && matches.length ? { invalid_characters: matches } : null;
        } else {
            return null;
        }
    }

}