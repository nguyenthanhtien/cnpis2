import { Driver } from './../models/Driver';
import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from '../../../../node_modules/rxjs';
import { DriverQueryResult } from 'src/app/core/models/Driver';
import { UrlConstants } from '../common/url.constants';
import { DriverCenterMasterData, KnownShipperModel } from '../models';

@Injectable()
export class DriverService {
    constructor(
        public dataService: DataService
    ) {
    }
    getDriverDetail(id: any) {
        return this.dataService.get(UrlConstants.API_GET_DRIVER_DETAIL + id);
    }
    getAllDriver(params: any): Observable<DriverQueryResult> {
        return this.dataService.post(UrlConstants.API_GET_DRIVER, JSON.stringify(params));
    }
    postDriver(data?: Driver): Observable<Driver> {
        return this.dataService.post(UrlConstants.API_POST_DRIVER, JSON.stringify(data));
    }
    putDriver(data?: Driver): any {
        return this.dataService.put(UrlConstants.API_PUT_DRIVER, JSON.stringify(data));
    }
    putStatusDriver(id: any): any {
        return this.dataService.put(UrlConstants.API_PUT_STATUS + id);
    }
    getAllUPSFacilityMasterData() {
        return this.dataService.get(UrlConstants.API_GETALL_UPSFACILITYMASTERDATA);
    }
    getUPSFacilityMasterDataById(id: any) {
        return this.dataService.get(UrlConstants.API_GET_UPSFACILITYMASTERDATA_BY_ID + id);
    }
    getAllKnownShipperMasterData(): Observable<KnownShipperModel[]> {
        return this.dataService.get(UrlConstants.API_GETALL_KNOWNSHIPPER);
    }
    public getAllDriverForShipment(): Observable<Driver[]> {
        return this.dataService.get(UrlConstants.API_GET_ALL_DRIVER);
    }
}