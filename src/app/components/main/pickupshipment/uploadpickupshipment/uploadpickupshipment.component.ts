import { Component, OnInit, ViewChild } from '@angular/core';

import { BsModalService} from 'ngx-bootstrap/modal';

import { ProgressHttp } from 'angular-progress-http';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { FormGroup } from '@angular/forms';
import { MessageBox, DataService, UploadService, NotificationService, UtilityService } from 'src/app/core/services';
import { UrlConstants } from 'src/app/core/common';
import { PickupShipmentUpload } from 'src/app/core/models';
import { saveAs } from 'file-saver'; 
import { AppConfig } from '../../../../app.config';
import { UploadpickupshipmentDetailComponent } from '../UploadpickupshipmentDetail/uploadpickupshipment-detail.component';
import { TranslatePipe } from '../../../../core/pipes/translate.pipe';
import { Router } from '@angular/router';

@Component({
  selector: 'app-uploadpickupshipment',
  templateUrl: './uploadpickupshipment.component.html',
  providers: [TranslatePipe, MessageBox]
})

export class UploadpickupshipmentComponent implements OnInit {
  private _isValid:boolean = false;
  private pickupShipmentDto: PickupShipmentUpload;
  private sortColumns = [];

  public pageList: number[] = [20, 25, 50, 100];
  public pageSize: number = 20;
  public currentPage: number = 1;
  public uploadFileName: string = '';
  public formUpload: FormGroup;
  public fileData: File[];
  public processPercent: number;
  public pickupShipmentModels: PickupShipmentUpload[] = [];
  public pager: any = {};
  public pagedItems: PickupShipmentUpload[] = [];
  public CreateBy : string = '';

  constructor(private dataService: DataService,
    private uploadService: UploadService,
    private processHttp: ProgressHttp,
    private notificationService: NotificationService,
    private utilityService: UtilityService,
    private _http: Http,
    private mdlService: BsModalService,
    private transPipe: TranslatePipe,
    private route: Router,
    private msgBox: MessageBox) {
  }

  ngOnInit() {
  }

  public get isValid() : boolean {
    let result : boolean = true;
    this.pickupShipmentModels.forEach((shipment: PickupShipmentUpload, index: number) => {
      if (!shipment.StatusRow) {
        result = false;
      }
    });
    return result;
  }

  public getFiles(fileProfiles: File[]): File[] {
    this.fileData = fileProfiles;
    if (fileProfiles.length > 0) {
      this.uploadFileName = this.fileData[0].name;
    }
    return this.fileData;
  }

  public showProgressBar(): boolean {
    return this.processPercent != null && this.processPercent > 0;
  }

  private checkValidFileType(fileName: string): boolean {
    var fileExtension = fileName.substr(fileName.lastIndexOf('.') + 1);
    return (fileExtension == 'csv' || fileExtension == 'xlsx'|| fileExtension == 'xls');
  }

  private compare(a,b): number {
    let result = 0;
    for (let i = 0; i < this.sortColumns.length; i++) {
      let sortItem = this.sortColumns[i];
      if (a[sortItem.property] < b[sortItem.property]) {
        result = (sortItem.isSort?-1:1);
        break;
      } else if (a[sortItem.property] > b[sortItem.property]) {
        result = (sortItem.isSort?1:-1);
        break;
      }
    }
    return result;
  }

  public sort(columnName: string): void {
    let isSort: boolean = false;
    var sortIndex = this.sortColumns.findIndex(function(sortItem, index) {
      return sortItem.property === columnName;
    });
    if (sortIndex > -1) {
      isSort = !this.sortColumns[sortIndex].isSort;
    }
    this.sortColumns.splice(0);
    this.sortColumns.push({ property: columnName, isSort: isSort });
    this.pickupShipmentModels.sort((a, b) => { return this.compare(a, b); });
    this.filter();
  }

  public postFileToServer(): void {
    const formData = new FormData();
    formData.append("Upload", this.fileData[0]);
    if (this.checkValidFileType(this.fileData[0].name)) {
      this.processHttp
        .withUploadProgressListener(process => {
          this.processPercent = process.percentage;
        })
        .withDownloadProgressListener(progress => {
          console.log(`Downloading ${progress.percentage}%`);
        })
        .post(AppConfig.BASE_API + UrlConstants.UPLOAD_PICKUP_SHIPMENT_FILE, formData)
        .map((res: Response) => res.json()).subscribe(
          (result: PickupShipmentUpload[]) => {
            if (result != null && result.length > 0) {
              result.forEach((shipment: PickupShipmentUpload, index: number) => {
                shipment.UpdatedBy = '00000000-0000-0000-0000-000000000000';
                shipment.Index = index;
                this.pickupShipmentModels.push(shipment);
                this.CreateBy = shipment.CreatedBy;
              });
            }
            this.setPage(1);
            this.resetProgress();
            this.sortColumns.splice(0, this.sortColumns.length);
          }, (err) => {
            this.notificationService.printErrorMessage(this.transPipe.transform('msg_file_format_invalid', null));
            this.resetProgress();
          });
    } else {
      this.notificationService.printErrorMessage(this.transPipe.transform('msg_file_format_invalid', null));
    }
  }

  private resetProgress(): void {
    this.processPercent = 0;
  }

  public downloadTemplateProfile() {
    const options = new RequestOptions({ responseType: ResponseContentType.Blob })
    return this._http.get(AppConfig.BASE_API + UrlConstants.DOWNLOAD_TEMPLATE_PICKUPSHIPMENT, options)
      .map((response: Response) => <Blob>response.blob()).subscribe(data => {
        const b = new Blob([data], { type: 'text/csv' });
        saveAs(b, UrlConstants.FILENAME_PICKUPSHIPMENT_TEMPLATE);
      });
  }

  public downloadReport(): void {
    let fileName: string = this.uploadFileName.substr(0, this.uploadFileName.lastIndexOf('.'));
    const options = new RequestOptions({ responseType: ResponseContentType.Blob })
    this._http.post(AppConfig.BASE_API + UrlConstants.PICKUPSHIPMENT_EXPORTREPORT, {
      'PickupShipments' : this.pickupShipmentModels,
      'UploadFileName' : fileName
    }, options)
    .map((response: Response) => <Blob>response.blob()).subscribe(data => {
      const b = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      saveAs(b, fileName + UrlConstants.FILENAME_PICKUPSHIPMENT_REPORT);
    });
  }

  private isvalidCreatedBy():boolean {
    let result: boolean = true;
    this.pickupShipmentModels.forEach((shipment: PickupShipmentUpload, index: number) => {
      if (shipment.CreatedBy == null || shipment.CreatedBy == '') {
        result = false;
      }
    });
    return result;
  }

  public postDataValidProfile() {
    if (!this.isvalidCreatedBy()) {
      this.notificationService.printErrorMessage(this.transPipe.transform('msg_createdby_notexists', null));
      return;
    }
    this.dataService.post(UrlConstants.POST_DATA_PROFILE, this.pickupShipmentModels)
    .subscribe(() => {
        this.notificationService.printSuccessMessage(this.transPipe.transform('MSG_15', null));
        this.route.navigate(['/main/pickupshipment']);
      }, (err:any) => {
        this.notificationService.printErrorMessage(this.transPipe.transform('MSG_16', null));
      });
  }

  public setPage(page: number): void {
    this.currentPage = page;
    this.filter();
  }

  public filter(): void {
    let startIndex = (this.currentPage - 1) * this.pageSize;
    let endIndex = parseInt(startIndex.toString()) + parseInt(this.pageSize.toString());
    if (endIndex > this.pickupShipmentModels.length) {
      endIndex = this.pickupShipmentModels.length;
    }
    this.pager = { startIndex : startIndex, endIndex: endIndex };
    this.pagedItems = this.pickupShipmentModels.slice(startIndex, endIndex);
  }

  private padStart(value: string, a: number, b: string): string {
    let s = value;
    for(var i = 0; i < a - s.length; i++) {
    	s = b + s;
    }
    return s;
  }

  private toLocateAppDate(value: string): string {
    let d: Date = new Date(value);
    return d.getFullYear() + '/' + this.padStart(d.getMonth().toString(),2,'0') + '/' 
           + this.padStart(d.getDate().toString(),2,'0')  +' ' 
           + this.padStart(d.getHours().toString(),2,'0') + ':' 
           + this.padStart(d.getMinutes().toString(),2,'0') + ':' 
           + this.padStart(d.getSeconds().toString(),2,'0');
  }

  private isDateTime(value: string): boolean {
    var result = value.match(/^\d{4}\/\d{1,2}\/\d{1,2} \d{1,2}\:\d{1,2}\:\d{1,2}$/);
    return (result != null && result.length > 0);
  }

  public editShipment(item: PickupShipmentUpload): void {
    this.pickupShipmentDto = item;
    let editShipment = Object.assign({}, this.pickupShipmentDto);
    if (!this.isDateTime(editShipment.PickUpDateTime)) {
      editShipment.PickUpDateTime = (new Date()).toString();
    }
    let mdlRef = this.mdlService.show(UploadpickupshipmentDetailComponent, { backdrop: 'static',
          initialState: { Shipment: editShipment, Shipments: this.pickupShipmentModels } });
    mdlRef.content.OnSave.subscribe((shipment: PickupShipmentUpload) => {
      this.pickupShipmentDto = Object.assign(this.pickupShipmentDto, shipment);
      this.pickupShipmentDto.PickUpDateTime = this.toLocateAppDate(this.pickupShipmentDto.PickUpDateTime);
    }, (err: any) => {
    });
  }

  public removeShipment(shipment: PickupShipmentUpload): void {
    let mdlRef = this.msgBox.confirm('Are you sure you want to DELETE shipment?', 'Confirm deleting shipment');
    mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
      let shipmentIndex = this.pickupShipmentModels.findIndex((value: PickupShipmentUpload, index: number)=>{
        return shipment.Index == value.Index;
      });
      this.pickupShipmentModels.splice(shipmentIndex, 1);
      this.filter();
    });
  }

  public updateCreator(creator: string): void {
    this.pickupShipmentModels.forEach((shipment: PickupShipmentUpload, index: number) => {
      shipment.CreatedBy = creator;
    });
  }

  public cancel(): void {
    this.fileData = [];
    this.uploadFileName = '';
    this.pickupShipmentModels.splice(0, this.pickupShipmentModels.length);
  }
}
