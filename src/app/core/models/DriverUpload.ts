import { Driver } from "./Driver";

export class DriverUpload {
    public DriverDto : Driver
    public IsInvalid : boolean
    public Message : string
    public ListData: Driver[];
    public TotalItems: number;
    public SucessedItems: number;
    public FailedItems: number;
    public ReportFilePath: string;
}