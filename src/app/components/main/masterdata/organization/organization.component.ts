import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../../../../core/services/data.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { KnownShipperModel } from '../../../../core/models/Organization';
import { MessageContstants } from '../../../../core/common/message.constants';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { KnownShipperService } from '../../../../core/api/masterdata.service';
import { MessageBox } from '../../../../core/services/modal.service';
import { TranslatePipe } from '../../../../core/pipes/translate.pipe';
import { error } from 'util';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  providers: [DataService, MessageBox, TranslatePipe, KnownShipperService]
})
export class OrganizationComponent implements OnInit {

  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  public knowShipper: KnownShipperModel[]=[];
  // public allListOrganizations: any;
  public listShipper: KnownShipperModel;
  public pageSize : number;
  public pageNumber : number;
  public orderBy : string;
  public orderType : boolean = false;
  public totalItems : number;
  public searchName : string;
  public searchAccount : string;
  public searchPhone : string;
  public searchTax : string;
  public rotate = true;
  public notFound : boolean;

  constructor(private msgBox: MessageBox,
    private pipeTranslate : TranslatePipe,
    private knowShipperService: KnownShipperService,
    public dataService: DataService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.pageSize = 20;
    this.pageNumber = 1;
    this.orderBy = 'CreateTime';
    this.orderType = false;
    this.getAll();
  }

  resetFormLoad(){
    this.searchName = '';
    this.searchAccount = '';
    this.searchPhone = null;
    this.searchTax = '';
    this.getAll();
  }

  public toggleSelect(event){
    this.knowShipper.forEach((item: KnownShipperModel) => {
      item.selected = event;
    });
  }

  public pageChanged($event: any){
    this.pageNumber = $event.page;
    this.getAll();
  }

  public sort(fieldName: string){
    if(this.orderBy == fieldName){
      this.orderType = !this.orderType;
    }else{
      this.orderBy = fieldName;
      this.orderType = false;
    }
    this.getAll();
  }

  showAddModal(): void{
    this.listShipper = new KnownShipperModel();
    this.modalAddEdit.show();
  }

  checkSpecialChar(event)
  {   
    let k;  
    k = event.charCode;
    return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
  }

  getAll() {
    let config = {
      PageNumber: this.pageNumber,
      pageSize: this.pageSize,
      OrderBy: this.orderBy,
      OrderType: this.orderType,
      KnownShipperName: this.searchName,
      ShipperAccount: this.searchAccount,
      PhoneNumber: this.searchPhone,
      TaxNo: this.searchTax
    }
    return this.knowShipperService.getShipper(config)
      .subscribe((res: any) =>
       {
        this.totalItems = res.TotalItems;
        if(this.totalItems > 0){
          this.knowShipper = res.ListData;
          this.notFound = false;
        }else{
          this.notFound = true;
        }
      },
        (err) => this.dataService.handleError(err));
  }

  saveShipper(form: NgForm) {
    if (this.listShipper.KnownShipperId == undefined) {
      this.knowShipperService.createShipper(JSON.stringify(this.listShipper))
      .subscribe(response => {
        this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-organization-create-ok',null));
        this.modalAddEdit.hide();
        this.getAll();
        form.resetForm();
      }, error => {
        if(error.status === 0){
          this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-create-err', null));
        }else{
          var res = error._body.split(",",2);
          switch(res[0]){
            case('"KnownShipperAccount'):
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-account-exists', null));
              break;
            case('"KnownShipperTaxNo'):
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-taxno-exists', null));
              break;
            default:
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-create-err', null));
          }
        }
        
      });
    }
    else {
      this.knowShipperService.updateShipper(JSON.stringify(this.listShipper))
      .subscribe(response => {
        this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-organization-update-ok',null));
        this.modalAddEdit.hide();
        this.getAll();
        form.resetForm();
      }, error => {
        if(error.status === 0){
          this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-update-err', null));
        }else{
          var res = error._body.split(",",2);
          switch(res[0]){
            case('"KnownShipperAccount'):
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-account-exists', null));
              break;
            case('"KnownShipperTaxNo'):
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-taxno-exists', null));
              break;
            default:
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-update-err', null));
          }
        }
      });
    }
  }


  showEditModal(id: string) {
    this.loadUserDetail(id);
    this.modalAddEdit.show();
  }

  loadUserDetail(id: string) {
    this.knowShipperService.getShipperId(id)
      .subscribe((response: KnownShipperModel) => {
        this.listShipper = response;
        console.log(this.listShipper);
      });
  }

  onDeleteShipper(id: any){
    let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-organization-body-delete',null), this.pipeTranslate.transform('msg-organization-title-delete',null));
    mdlRef.content.OnResult.subscribe((msgBoxResult: any) =>{
    this.knowShipperService.deleteShipperId(id)
      .subscribe(x => {
        this.getAll();
        this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-organization-delete-ok',null));
      }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-delete-err',null)));
    });
  }

  public deleteSelected(item: any[] = []){
    item = this.knowShipper.filter((c: KnownShipperModel, index: number) => { return c.selected; })
      .map((p: KnownShipperModel, index: number) => { return p.KnownShipperId; });
      if (item != null && item.length > 0){
        let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-organization-body-delete',null), this.pipeTranslate.transform('msg-organization-title-delete',null));
        mdlRef.content.OnResult.subscribe((msgBoxResult: any) =>{
          this.knowShipperService.multipleDeleteShipper(item)
          .subscribe(x => {
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-organization-delete-ok',null));
            this.getAll();
          }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-delete-err',null)));
        });
      }else{
        this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-organization-noitem-delete',null));
      }
  }
}
