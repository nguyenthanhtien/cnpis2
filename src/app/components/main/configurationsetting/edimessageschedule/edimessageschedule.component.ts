import { Component, OnInit } from '@angular/core';
import { Time } from "@angular/common";

import { EDIMessageConfiguration } from '../../../../core/models/EDIMessageConfiguration';
import { DataService } from '../../../../core/services/data.service';
import { Timestamp } from 'rxjs/Rx';
import { Type } from '@angular/compiler';

@Component({
  selector: 'app-edimessageschedule',
  templateUrl: './edimessageschedule.component.html',
})

export class EdimessagescheduleComponent implements OnInit {
  public EDIMessageConfigurations: EDIMessageConfiguration[] = [];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    let msg3102: EDIMessageConfiguration = new EDIMessageConfiguration();
    msg3102.MessageID = '3102';
    msg3102.MessageName = '3102 机构用户信息数据元';
    msg3102.RepeatHour = 3;
    msg3102.IsRunning = true;
    msg3102.BeginTime = new Date();
    msg3102.EndTime = new Date();
    msg3102.RepeatDays = { Monday : true, TuesDay : true, Wednesday : true,
                           Thursday : true, Friday : true, Saturday : true, Sunday : true };
    this.EDIMessageConfigurations.push(msg3102);
  }

  public save(): void {
    alert('OK');
  }
  
  public display(time: any): void {

  }
}
