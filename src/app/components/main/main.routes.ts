import { Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/guard.service';

export const mainRoutes: Routes = [

    { path: '', redirectTo: 'main', pathMatch: 'full', canActivate: [AuthGuardService] },

    { path: 'driver', loadChildren: './driver/driver.module#DriverModule', canActivate: [AuthGuardService] },

    { path: 'pickupshipment', loadChildren: './pickupshipment/pickupshipment.module#PickupshipmentModule', canActivate: [AuthGuardService] },

    { path: 'user', loadChildren: './user/user.module#UserModule', canActivate: [AuthGuardService] },

    { path: 'masterdata', loadChildren: './masterdata/masterdata.module#MasterdataModule', canActivate: [AuthGuardService] },

    { path: 'configurationsetting', loadChildren: './configurationsetting/configurationsetting.module#ConfigurationsettingModule', canActivate: [AuthGuardService] },

    { path: 'editransmission', loadChildren: './editransmission/editransmission.module#EditransmissionModule', canActivate: [AuthGuardService] }

]
