import { Routes } from '@angular/router';
import { UploadpickupshipmentComponent } from './uploadpickupshipment/uploadpickupshipment.component';
import { PickupshipmentComponent } from './pickupshipment.component';
import { AUTHGROUP } from 'src/app/core/common';
import { RoleGuardService } from 'src/app/core/services';

export const mainRoutes: Routes = [
    { path: '', component: PickupshipmentComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER,AUTHGROUP.SYSTEM_ADMIN] }},

    { path: 'upload-pickup', component: UploadpickupshipmentComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER,AUTHGROUP.SYSTEM_ADMIN] }}

]