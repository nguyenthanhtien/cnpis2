import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { AuthService, NotificationService } from 'src/app/core/services';
import { TranslateModule } from '../../core/pipes';
export const routes: Routes = [
  { path: '', component: LoginComponent }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  providers: [AuthService, NotificationService],
  declarations: [LoginComponent]
})
export class LoginModule { }
