import { Component, OnInit } from '@angular/core';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';

@Component({
  selector: 'app-leftmenu',
  templateUrl: './leftmenu.component.html',
  styleUrls: ['./leftmenu.component.css'],
  providers : [TranslatePipe]
})
export class LeftmenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
