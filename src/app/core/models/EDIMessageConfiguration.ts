export class EDIMessageConfiguration {
    public MessageID: string;
    public MessageName: string;
    public IsRunning: boolean;
    public BeginTime: Date;
    public EndTime: Date;
    public RepeatHour: number;
    public RepeatDays: {
        Monday: boolean;
        TuesDay: boolean;
        Wednesday: boolean;
        Thursday: boolean;
        Friday: boolean;
        Saturday: boolean;
        Sunday: boolean;
    };
}