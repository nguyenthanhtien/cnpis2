import { Component, Injectable, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'message-box',
  templateUrl: './modal/message-box.component.html'
})

export class MessageBoxComponent implements OnInit {
  @Input() Message: string;
  @Input() Caption: string;
  @Input() MessageBoxType: number;
  @Output() OnResult: EventEmitter<any> = new EventEmitter<any>();
  @Output() OnDismiss: EventEmitter<any> = new EventEmitter<any>();

  constructor(public mdlRef: BsModalRef) {
  }

  ngOnInit() {
  }

  public setResult(): void {
    this.mdlRef.hide();
    this.OnResult.emit('OK');
  }

  public close(): void {
    this.mdlRef.hide();
    this.OnDismiss.emit('Cancel');
  }
}

const MB_OK: number = 1;
const MB_OKCancel: number = 2;

@Injectable()
export class MessageBox {
  constructor(private mdlService: BsModalService) {
  }

  public alert(message: string, content?: string): BsModalRef {
    let msgBoxData = { 'Message': message, 'Caption': content, 'MessageBoxType': MB_OK };
    let mdlRef = this.mdlService.show(MessageBoxComponent, { backdrop: 'static', initialState: msgBoxData });
    return mdlRef;
  }

  public confirm(message: string, content?: string): BsModalRef {
    let msgBoxData = { 'Message': message, 'Caption': content, 'MessageBoxType': MB_OKCancel };
    let mdlRef = this.mdlService.show(MessageBoxComponent, { backdrop: 'static', initialState: msgBoxData });
    return mdlRef;
  }
}