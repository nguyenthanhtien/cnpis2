import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';
import { UtilityService } from './utility.service';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import { MessageContstants } from '../common/message.constants';
import { AppConfig } from '../../app.config';

@Injectable()
export class DataService {
  private headers: Headers;
  constructor(private _http: Http, private _router: Router,
    private _notificationService: NotificationService, private _utilityService: UtilityService) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.activeAuthorization(AppConfig.ACTIVE_AUTHORIZATION);
  }
  activeAuthorization(active: boolean) {
    if (active) {
      let userKey = this.headers.get(AppConfig.CT_REMOTE_USER);
      this.headers.append(AppConfig.CT_REMOTE_USER, userKey);
    }
  }
  get(uri: string) {
    return this._http.get(AppConfig.BASE_API + uri).map(this.extractData);
  }
  public getDataOnPage(uri: string, pageNumber: number, pageSize: number, condition: any) {
    let params = { 'PageNumber': pageNumber.toString(), 'PageSize': pageSize.toString(), 'Condition': condition };
    return this._http.get(AppConfig.BASE_API + uri + "/search", { params: params, headers: this.headers }).map(this.extractData);
  }

  post(uri: string, data?: any) {
    return this._http.post(AppConfig.BASE_API + uri, data, { headers: this.headers }).map(this.extractData);
  }
  put(uri: string, data?: any) {
    return this._http.put(AppConfig.BASE_API + uri, data );
  }



  delete(uri: string, key: string, id: string) {
    return this._http.delete(AppConfig.BASE_API + uri, { headers: this.headers });
  }

  deleteMultiple(uri: string, ids: string[]) {
    return this._http.delete(AppConfig.BASE_API + uri, { body: { 'IDs': ids }, headers: this.headers });
  }

  deleteWithMultiParams(uri: string, params) {
    var paramStr: string = '';
    for (let param in params) {
      paramStr += param + "=" + params[param] + '&';
    }
    return this._http.delete(AppConfig.BASE_API + uri + "/?" + paramStr, { headers: this.headers })
      .map(this.extractData);
  }

  postFile(uri: string, data?: any) {
    return this._http.post(AppConfig.BASE_API + uri, data, { headers: this.headers })
      .map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  public handleError(error: any) {
    if (error.status == 401) {
      this._notificationService.printErrorMessage(MessageContstants.UNAUTHORIZED);
      this._utilityService.navigateToLogin();
    }
    else if (error.status == 403) {
      this._notificationService.printErrorMessage(MessageContstants.FORBIDDEN);
      this._utilityService.navigateToLogin();
    }
    else if (error.status == 404) {
      this._notificationService.printErrorMessage(MessageContstants.APINOTFOUND);
    }
    else {
      let errMsg = JSON.parse(error._body).Message;
      this._notificationService.printErrorMessage(errMsg);
      return Observable.throw(errMsg);
    }
  }
}
