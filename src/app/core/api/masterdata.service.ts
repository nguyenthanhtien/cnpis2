import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/core/services';
import { KnownShipperModel, County, CityQueryResult, City } from 'src/app/core/models';
import { UrlConstants } from 'src/app/core/common';
import { ProvinceMasterData, ProvinceQueryResult } from '../models/ProvinceMasterData';
import { CountyQueryResult } from 'src/app/components/main/masterdata/county/countyQueryResult';
import { DriverCenterMasterData, DriverCentersQueryResult } from 'src/app/core/models/DriverCenterMasterData';

@Injectable()
export class KnownShipperService {
    constructor(public dataService: DataService) { }

    getShipper(data: any): Observable<KnownShipperModel> {
        return this.dataService.post(UrlConstants.GET_SHIPPER, data);
    }

    createShipper(data: any): Observable<KnownShipperModel> {
        return this.dataService.post(UrlConstants.MODIFY_SHIPPER, data);
    }

    updateShipper(data: any): Observable<any> {
        return this.dataService.put(UrlConstants.MODIFY_SHIPPER, data);
    }

    getShipperId(id): Observable<KnownShipperModel> {
        return this.dataService.get(UrlConstants.GETID_SHIPPER + id);
    }

    deleteShipperId(id: any) {
        return this.dataService.delete(UrlConstants.GETID_SHIPPER + id.toString(), 'id', id.toString());
    }

    multipleDeleteShipper(ids): any {
        return this.dataService.deleteMultiple(UrlConstants.MULTIPLE_DELETE_SHIPPER, ids);
    }


}
@Injectable()
export class ProvinceService {
    constructor(public dataService: DataService) {

    }
    getDetailProvince(id: string): Observable<ProvinceMasterData> {
        return this.dataService.get(UrlConstants.PROVINCE_DETAIL + id);
    }
    getAllProvince(params: any): Observable<ProvinceQueryResult> {
        return this.dataService.post(UrlConstants.PROVINCE_SEARCH, params);
    }

    postCreateProvince(data?: any): Observable<ProvinceMasterData> {
        return this.dataService.post(UrlConstants.PROVINCE_CREATE, data);
    }

    putUpdateProvince(data?: any): Observable<any> {
        return this.dataService.put(UrlConstants.PROVINCE_UPDATE, data);
    }

    deleteProvince(id: string): any {
        return this.dataService.delete(UrlConstants.PROVINCE_DELETE + id, 'UserID', id);
    }

    deleteMultiProvince(params: any): any {
        return this.dataService.deleteMultiple(UrlConstants.PROVINCE_DELETEMULTI, params);
    }
}

@Injectable()
export class  CountyService {
    constructor(public dataService: DataService) { }

    getAllCounty(data: any): Observable<CountyQueryResult> {
        return this.dataService.post(UrlConstants.API_COUNTY_SEARCH, data);
    }

    createCounty(data: any): Observable<any> {
        return this.dataService.post(UrlConstants.API_COUNTY_CREATE, data);
    }

    updateCounty(data: any): Observable<any> {
        return this.dataService.put(UrlConstants.API_COUNTY_UPDATE, data);
    }

    getCountyId(id): Observable<any> {
        return this.dataService.get(UrlConstants.API_COUNTY_DETAIL + id);
    }

    deleteCountyById(id: any) {
        return this.dataService.delete(UrlConstants.API_COUNTY_DELETE + id.toString(), 'id', id.toString());
    }

    multipleDeleteCounty(ids: any): any {
        return this.dataService.deleteMultiple(UrlConstants.API_COUNTY_DELETEMULTI, ids);
    }
    getAllCityInCounty(): any {
        return this.dataService.get(UrlConstants.API_COUNTY_CITY_GETALL);
    }
    getAllProvince(): any {
        return this.dataService.get(UrlConstants.API_CITY_GET_PROVINCE);
    }
}

@Injectable()
export class CityService {
    constructor(public dataService: DataService) { }

    getAllCity(data: any): Observable<CityQueryResult> {
        return this.dataService.post(UrlConstants.API_CITY_SEARCH, data);
    }

    createCity(data: any): Observable<CityQueryResult> {
        return this.dataService.post(UrlConstants.API_CITY_CREATE, data);
    }

    updateCity(data: any): Observable<any> {
        return this.dataService.put(UrlConstants.API_CITY_UPDATE, data);
    }

    getCityById(id: string): Observable<any> {
        return this.dataService.get(UrlConstants.API_CITY_DETAIL + id);
    }

    deleteCityById(id: any) {
        return this.dataService.delete(UrlConstants.API_CITY_DELETE + id.toString(), 'id', id.toString());
    }

    multipleDeleteCity(ids: any): any {
        return this.dataService.deleteMultiple(UrlConstants.API_CITY_DELETEMULTI, ids);
    }
    getAllProvince(): any {
        return this.dataService.get(UrlConstants.API_CITY_GET_PROVINCE);
    }
    getAllCityInProvince(id: any): any {
        return this.dataService.get(UrlConstants.API_CITY_IN_PROVINCE + id);
    }
}
@Injectable()
export class DriverCenterService {
    constructor(public dataService: DataService) {

    }
    getAllCity(): any {
        return this.dataService.get(UrlConstants.CITY_GETALL);
    }

    getAllCouty(): Observable<CountyQueryResult> {
        return this.dataService.get(UrlConstants.GET_ALL_COUNTY);
    }

    getAllProvince(): Observable<CountyQueryResult> {
        return this.dataService.get(UrlConstants.GET_ALL_PROVINCE);
    }
    getAllDriverList(): any {
        return this.dataService.get(UrlConstants.GET_ALL_DRIVER_LIST);
    }
    getAllDriverListById(id: any): any {
        return this.dataService.get(UrlConstants.GET_ALL_DRIVER_LIST_BY_ID + id);
    }
    getAllCountyByCity(id: any): any {
        return this.dataService.get(UrlConstants.GET_ALL_COUNTY_BY_CITY + id);
    }
    getDetailDriverCenter(id: string): Observable<DriverCenterMasterData> {
        return this.dataService.get(UrlConstants.DRIVER_CENTERS_DETAIL + id);
    }
    getAllDriverCenter(params: any): Observable<DriverCentersQueryResult> {
        return this.dataService.post(UrlConstants.DRIVER_CENTERS_SEARCH, params);
    }

    postCreateDriverCenter(data?: any): Observable<DriverCenterMasterData> {
        return this.dataService.post(UrlConstants.DRIVER_CENTERS_CREATE, data);
    }

    putUpdateDriverCenter(data?: any): Observable<any> {
        return this.dataService.put(UrlConstants.DRIVER_CENTERS_UPDATE, data);
    }

    deleteDriverCenter(id: string): any {
        return this.dataService.delete(UrlConstants.DRIVER_CENTERS_DELETE + id, 'UserID', id);
    }

    deleteMultiDriverCenter(params: any): any {
        return this.dataService.deleteMultiple(UrlConstants.DRIVER_CENTERS_DELETEMULTI, params);
    }
}
