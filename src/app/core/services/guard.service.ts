import { CanActivate, Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import { UtilityService } from "./utility.service";

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(
        public _authService: AuthService,
        public _router: Router,
        public _utilityService: UtilityService) {
    }
    canActivate(): boolean {
        if (!this._authService.isAuthenticated()) {
            this._utilityService.navigateToLogin();
            return false;
        }
        return true;
    }
}