import { Routes } from '@angular/router';
import { UserComponent } from './user.component';
import { RoleGuardService } from 'src/app/core/services';
import { AUTHGROUP } from 'src/app/core/common';

export const mainRoutes: Routes = [
    {
        path: '', component: UserComponent, canActivate: [RoleGuardService], data : { expectedRole: [AUTHGROUP.SYSTEM_ADMIN,AUTHGROUP.DISTRICT_ADMIN]}
    }

]