import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CountyService, ProvinceService, CityService } from '../../../../../core/api';
import { TranslatePipe } from '../../../../../core/pipes/translate.pipe';
import { City, ProvinceMasterData, County, CountyModelAdd } from '../../../../../core/models';
import { NotificationService } from '../../../../../core/services/notification.service';
import { DataService } from '../../../../../core/services/data.service';
import { MessageBox } from '../../../../../core/services';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'county-detail',
  templateUrl: './county-detail.component.html',
  providers: [DataService, TranslatePipe]
})

export class CountyDetailComponent implements OnInit {

  @Input() county: County;
  @Output() OnSaveChanged: EventEmitter<County> = new EventEmitter<County>();

  public cities: City[] = [];
  public provinceMasterData: ProvinceMasterData[] = [];
  public modelCounty: CountyModelAdd;

  //
  public registerForm: FormGroup;
  public submitted: boolean;

  constructor(
    public mdlRef: BsModalRef,
    private notificationService: NotificationService,
    private provinceService: ProvinceService,
    private cityService: CityService,
    private pipeTranslate: TranslatePipe,
    private msgBox: MessageBox,
    private formBuilder: FormBuilder,
    private coutyService: CountyService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      ProvinceName: ['', [Validators.required, Validators.maxLength(255)]],
      ProvinceCode: ['', [Validators.required, Validators.maxLength(32)]],
      CityName: ['', [Validators.required, Validators.maxLength(255)]],
      CityCode: ['', [Validators.required, Validators.maxLength(32)]],
      CountyName: ['', [Validators.required, Validators.maxLength(255)]],
      CountyCode: ['', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(32)]],
      ProvinceId: [''],
      CityId: [''],
      CountyId: [''],
    });
    this.submitted = false;
    this.getAllProvince();
    this.getAllCities();
    if (this.county) {
      this.countyFrm.CountyId.setValue(this.county.CountyId);
      this.countyFrm.CountyName.setValue(this.county.CountyName);
      this.countyFrm.CountyCode.setValue(this.county.CountyCode);
      this.provinceService.getDetailProvince(this.county.ProvinceId).subscribe((res: any) => {
        this.countyFrm.ProvinceId.setValue(res.ProvinceId);
        this.countyFrm.ProvinceName.setValue(res.ProvinceName);
        this.countyFrm.ProvinceCode.setValue(res.ProvinceCode);
      });
      this.cityService.getCityById(this.county.CityId).subscribe((res: any) => {
        this.countyFrm.CityId.setValue(res.CityId);
        this.countyFrm.CityName.setValue(res.CityName);
        this.countyFrm.CityCode.setValue(res.CityCode);
      });
    }
  }

  get countyFrm() {
    return this.registerForm.controls;
  }

  public getAllProvince(): void {
    this.cityService.getAllProvince().subscribe((response: any) => {
      this.provinceMasterData = response;
    }, () => {
      this.errorMessage('msg-city-province-error-api');
    });
  }

  public getAllCities(): void {
    this.coutyService.getAllCityInCounty().subscribe((response: any) => {
      this.cities = response;
    }, () => {
      this.errorMessage('msg-county-get-all-city-error-api');
    });
  }

  public onItemChangeProvinceCode($event) {
    if (_.includes(this.provinceMasterData, $event.item)) {
      this.countyFrm.ProvinceCode.setValue($event.item.ProvinceCode);
      this.countyFrm.ProvinceId.setValue($event.item.ProvinceId);
      this.countyFrm.CityName.setValue('');
      this.cityService.getAllCityInProvince($event.item.ProvinceId).subscribe((res: any) => {
        this.cities = res;
      });
    } else {
      this.countyFrm.ProvinceName.setValue('');
    }
  }

  public onItemChangeCityCode($event) {
    if (_.includes(this.cities, $event.item)) {
      this.countyFrm.CityCode.setValue($event.item.CityCode);
      this.countyFrm.CityId.setValue($event.item.CityId);
    } else {
      this.countyFrm.CityName.setValue('');
    }
  }

  public onSaveCounty(): void {
    this.submitted = true;
    if (this.registerForm.valid === true) {
      const obj = this.registerForm.getRawValue();
      if (obj.CountyId === '') {
        this.coutyService.createCounty(obj).subscribe((response: any) => {
          this.OnSaveChanged.emit(response);
          this.mdlRef.hide();
          this.successMessage('msg-51-county');
        }, error => {
          this.errorProgress(error, 'msg-52-county');
        });
      } else {
        const mdlRef = this.msgBox.confirm(
          this.pipeTranslate.transform('msg-city-record', null),
          this.pipeTranslate.transform('msg-city-confirm-update', null)
        );
        mdlRef.content.OnResult.subscribe(() => {
          this.coutyService.updateCounty(obj).subscribe((res: any) => {
            this.OnSaveChanged.emit(res);
            this.successMessage('msg-53-county');
          }, err => {
            this.errorProgress(err, 'msg-54-county');
          });
        });
      }
    }
  }

  public errorProgress(error: any, msgError: any) {
    if (error) {
      switch (error['_body']) {
        case '"CountyCode, 1111"': {
          this.errorMessage('msg-6-county');
          break;
        }
        default:
          this.errorMessage(msgError);
          break;
      }
    }
  }

  public errorMessage(message: any) {
    this.notificationService.printErrorMessage(
      this.pipeTranslate.transform(message, null));
  }

  public successMessage(message: any) {
    this.notificationService.printSuccessMessage(
      this.pipeTranslate.transform(message, null));
  }

  public close(): void {
    this.submitted = false;
    this.registerForm.reset();
    this.mdlRef.hide();
  }
}
