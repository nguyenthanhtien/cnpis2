export class PickupShipmentUpload {
    public Index: number;
    public TrackingNo: string;
    public PickUpDateTime: string;
    public DriverEmployeeId: string;
    public PackageType: string;
    public DriverNationId: string;
    public ShipperUPSAccNum: string;
    public PickUpAddress: string;
    public PostCode: string;
    public PackageDescription: string;
    public PackageQuantity: string;
    public ConsigneeMobileNumber: string;
    public StreetNum: string;
    public SuiteNum: string;
    public BldgFloorNum: string;
    public MessageErrorDescription: string;
    public StatusRow: boolean;
    public CreatedBy: string;
    public UpdatedBy: string;
}