import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from '../../../../node_modules/rxjs';
import { UserQueryResult } from 'src/app/core/models/User';
import { UrlConstants} from '../common/url.constants';
import { User} from '../models/User';

@Injectable()
export class UserService {
    constructor(public dataService: DataService
    ) { }

    getDetailUser(id : string) : Observable<User> {
        return this.dataService.get(UrlConstants.USER_DETAIL + id);
    }

    getAllUser(params: any): Observable<UserQueryResult> {
        return this.dataService.post(UrlConstants.USER_SEARCH, params);
    }

    getAllGroupUser(): any {
        return this.dataService.get(UrlConstants.GROUPUSER_GETALL);
    }

    getAllFacility(): any {
        return this.dataService.get(UrlConstants.FACILITY_GETALL);
    }

    postCreateUser(data?: any): Observable<User> {
        return this.dataService.post(UrlConstants.USER_CREATE, data);
    }

    putUpdateUser(data?: any): any {
        return this.dataService.put(UrlConstants.USER_UPDATE, data);
    }

    deleteUser(id : string): any {
        return this.dataService.delete(UrlConstants.USER_DELETE + id, 'UserID', id)
    }

    deleteMultiUser(params : any): any {
        return this.dataService.deleteMultiple(UrlConstants.USER_DELETEMULTI , params)
    }

    putUpdateStatusUser(id : string): any {
        return this.dataService.get(UrlConstants.USER_UPDATESTATUS + id);
    }
}