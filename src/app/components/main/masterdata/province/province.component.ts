import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataService } from '../../../../core/services/data.service';
import { ProvinceMasterData, ProvinceQueryResult } from '../../../../core/models/ProvinceMasterData';
import { NotificationService } from '../../../../core/services/notification.service';
import { Subscription } from 'rxjs';
import { MessageBox } from 'src/app/core/services/modal.service';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { ProvinceService } from '../../../../core/api/masterdata.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-province',
  templateUrl: './province.component.html',
  providers: [DataService, MessageBox, TranslatePipe]
})

export class ProvinceComponent implements OnInit, OnDestroy {
  @ViewChild('modalAdd') modalAdd: ModalDirective;
  @ViewChild('modalEdit') public modalEdit: ModalDirective;
  public province: ProvinceMasterData;
  public subcription: Subscription;
  public pageSize: number;
  public pageNumber: number;
  public sortColumn: string = 'ProvinceCode';
  public isSorted: boolean = true;
  public searchCondition: ProvinceMasterData = new ProvinceMasterData();
  public provinces: ProvinceMasterData[] = [];
  public totalItems: number;
  public condition: ProvinceMasterData = new ProvinceMasterData();
  public selectedIDs: any[] = [];
  public noResultFound: boolean = false;
  public regexpUser: boolean;
  public searchProvinceCode: string;
  public searchProvinceName: string;
  public registerForm: FormGroup;
  public submitted: boolean;
  constructor(
    private dataService: DataService,
    private notificationService: NotificationService,
    private msgBox: MessageBox,
    private provinceService: ProvinceService,
    private pipeTranslate: TranslatePipe,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.pageSize = 20;
    this.pageNumber = 1;
    this.submitted = false;
    this.search();
    this.createForm();
  }

  ngOnDestroy() {
    if (this.subcription) {
      this.subcription.unsubscribe();
    }
  }

  public createForm() {
    this.registerForm = this.formBuilder.group({
      ProvinceId: [''],
      ProvinceName: ['', [Validators.required, Validators.maxLength(255)]],
      ProvinceCode: ['', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(32)]]
    });
  }
  get cityFrm() {
    return this.registerForm.controls;
  }

  resetForm() {
    this.searchProvinceCode = null;
    this.searchProvinceName = null;
    this.search();
  }
  public sort(columnName: any): void {
    if (this.sortColumn == columnName) {
      this.isSorted = !this.isSorted;
    } else {
      this.sortColumn = columnName;
      this.isSorted = false;
    }
    this.search();
  }
  public pageChanged($event: any): void {
    this.pageNumber = $event.page;
    this.search();
  }
  public search(): void {
    let params = {
      'PageSize': this.pageSize,
      'PageNumber': this.pageNumber,
      'OrderBy': this.sortColumn,
      'OrderType': this.isSorted,
      'ProvinceCode': this.searchProvinceCode,
      'ProvinceName': this.searchProvinceName,
    };
    this.provinceService.getAllProvince(params).subscribe(
      (queryResult: ProvinceQueryResult) => {
        this.totalItems = queryResult.TotalItems;
        if (this.totalItems > 0) {
          this.noResultFound = false;
          this.provinces = queryResult.ListData;
        } else {
          this.noResultFound = true;
        }
      }, (err: any) => { });
  }


  public ShowCreate(): void {
    this.province = new ProvinceMasterData();
    this.modalAdd.show();
  }
  public showEdit(id: string): void {
    this.getAllProvinceById(id);
    this.modalEdit.show();
  }

  public onResetProvince() {
    this.submitted = false;
    this.registerForm.reset();
    this.modalAdd.hide();
  }
  public onReSetEditProvince() {
    this.submitted = false;
    this.registerForm.reset();
    this.modalEdit.hide();
  }
  public getAllProvinceById(id: any){
    this.provinceService.getDetailProvince(id).subscribe((response: any)=>{
      this.cityFrm.ProvinceName.setValue(response.ProvinceName);
      this.cityFrm.ProvinceCode.setValue(response.ProvinceCode);
      this.cityFrm.ProvinceId.setValue(response.ProvinceId);
    })
  }
  public saveChangeDataProvince() {
    this.submitted = true;
    if (this.registerForm.valid == true) {
      const obj = this.registerForm.getRawValue();
      if (!obj.ProvinceId) {
        this.provinceService.postCreateProvince(obj).subscribe(
          res => {
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-add-ok', null));
            this.onResetProvince();
            this.search();
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-connection', null))
            }
            if (err._body.indexOf('PronvinceCode, 1111') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-province-check-exist', null))

            }
          });
      } else {
        let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-province-body-update', null), this.pipeTranslate.transform('msg-user-title-update', null));
        mdlRef.content.OnResult.subscribe(() => {
          this.provinceService.putUpdateProvince(obj).subscribe((response: any) => {
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-update-ok', null));
            this.onReSetEditProvince();
            this.search();
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-connection', null))
            }
            if (err._body.indexOf('PronvinceCode, 1111') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-province-check-exist', null))
            }
          });
        })
      }
    }
  }
  public deleteDataProvince(id: string): void {
    let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-province-body-delete', null), this.pipeTranslate.transform('msg-province-title-delete', null));
    mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
      if (id != null) {
        this.provinceService.deleteProvince(id)
          .subscribe((res: any) => {
            this.search();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-delete-ok', null));
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-connection', null))
            }
            if (err._body.indexOf('ProvinceId, 113') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-can-not-delete', null));
            }
          });
      }
    }, (err: any) => { });
  }

  public deleteMultiDataProvince(): void {
    this.selectedIDs = this.provinces.filter((province: ProvinceMasterData, index: number) => { return province.IsSelected; })
      .map((p: ProvinceMasterData, index: number) => { return p.ProvinceId; });
    if (this.selectedIDs != null && this.selectedIDs.length > 0) {
      let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-province-body-delete', null), this.pipeTranslate.transform('msg-province-title-delete', null));
      mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
        this.provinceService.deleteMultiProvince(this.selectedIDs)
          .subscribe((res: any) => {
            this.search();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-delete-ok', null));
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-connection', null))
            }
            if (err._body.indexOf('PronvinceModel, 1112') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-can-not-multidelete', null))
            }
          });
      }, (err: any) => {
        this.notificationService.printErrorMessage(err);
      });
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-not-1-fount', null))
    }
  }

  public selectAll(checked: boolean): void {
    this.provinces.forEach((province: ProvinceMasterData, index: number) => {
      province.IsSelected = checked;
    });
  }
}
