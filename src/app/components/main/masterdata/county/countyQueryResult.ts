import { County } from '../../../../core/models/County';

export class CountyQueryResult {
  public ListData: County[];
  public TotalItems: number;
}
