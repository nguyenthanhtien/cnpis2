import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './shared/404/pagenotfound.component';
import { AuthGuardService } from 'src/app/core/services';


export const appRoutes: Routes = [

    { path: '', redirectTo: 'login', pathMatch: 'full' },

    { path: 'login', loadChildren: './components/login/login.module#LoginModule' },

    { path: 'main', loadChildren: './components/main/main.module#MainModule', canActivate: [AuthGuardService] },

    { path: '**', component: PageNotFoundComponent }

];
