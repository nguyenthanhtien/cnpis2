import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Response } from '@angular/http';

import { BsModalRef } from 'ngx-bootstrap/modal';

import { PickupShipmentUpload } from '../../../../core/models/PickupShipmentUpload';
import { DriverService } from '../../../../core/api/driver.service';
import { Driver } from '../../../../core/models/Driver';
import { ShipmentCustomValidators } from '../shipment.validators';
import { PickupShipmentService } from '../../../../core/api/pickupshipment.service';
import { CustomValidators } from '../../../../core/validations/custom.validator';
import { uniqueTrackingNumberValidator } from '../uniqueTrackingNumber.directive';

@Component({
    selector: 'uploadpickupshipment-detail',
    templateUrl: './uploadpickupshipment-detail.component.html',
    providers: [DriverService]
})

export class UploadpickupshipmentDetailComponent implements OnInit {
    @Input() Shipment: PickupShipmentUpload;
    @Input() Shipments: PickupShipmentUpload[];
    @Output() OnSave: EventEmitter<PickupShipmentUpload> = 
                        new EventEmitter<PickupShipmentUpload>();
    
    public selectedDriverEmpId: string;
    public shipmentDate: Date;
    public submitted: boolean = false;
    public updateForm: FormGroup;
    public Drivers: Driver[] = [];
    public errors: { [id: string] : string } = {};
    public settings = {
        bigBanner: true,
        timePicker: true,
        format: 'yyyy/MM/dd hh:mm:ss',
        defaultOpen: false
    };

    constructor(public mdlRef: BsModalRef, private driverService: DriverService, 
        private pickupService: PickupShipmentService,
        private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.shipmentDate = new Date(this.Shipment.PickUpDateTime);
        this.selectedDriverEmpId = this.Shipment.DriverEmployeeId;
        this.updateForm = this.formBuilder.group({
            trackingno : ['', [Validators.required, ShipmentCustomValidators.isTrackingNumber, uniqueTrackingNumberValidator(this.Shipment, this.Shipments)]],
            pickupdatetime: ['', [Validators.required]],
            driveremployeeid: ['', [Validators.required, Validators.maxLength(16)]],
            drivernationid: ['', [Validators.required, Validators.maxLength(35)]],
            shipperupsaccnum : ['',[Validators.required, Validators.maxLength(6)]],
            pickupaddress : ['',[Validators.required, Validators.maxLength(300)]],
            postcode : ['',[Validators.required, Validators.maxLength(6)]],
            packagedescription : ['',[Validators.required, Validators.maxLength(300)]],
            packagequantity : ['',[Validators.required, CustomValidators.isNumberic]],
            consigneemobilenumber : ['',[Validators.required, Validators.maxLength(12), CustomValidators.isNumberic]],
            packagetype : ['', [Validators.required, ShipmentCustomValidators.validPackageType]]
        });
        this.driverService.getAllDriverForShipment().subscribe((result: Driver[]) => {
            this.Drivers = result;
        }, (err: any) => {
            this.mdlRef.hide();
        });
    }

    public get f() { return this.updateForm.controls; }

    public onDriverEmpIdChange($event: any): void {
        this.Shipment.DriverEmployeeId = $event.item.DriverUPSEmpId;
        let driver = this.Drivers.find((value: Driver, index: number) => {
            return value.DriverUPSEmpId == this.Shipment.DriverEmployeeId;
        });
        this.Shipment.DriverNationId = driver.DriverNationId;
    }

    public onShipmentDateChanged(date: any): void {
        this.Shipment.PickUpDateTime = date.toString();
        let zoneIndex = this.Shipment.PickUpDateTime.indexOf('(');
        if (zoneIndex >= 0) {
            this.Shipment.PickUpDateTime = this.Shipment.PickUpDateTime.substr(0, zoneIndex).trim();
        }
    }

    public onSubmit(): void {
        this.submitted = true;
        if (this.updateForm.invalid) {
            return;
        }
        this.pickupService.CheckShipment(this.Shipment).subscribe(() => {
            this.mdlRef.hide();
            this.Shipment.StatusRow = true;
            this.Shipment.MessageErrorDescription = '';
            this.OnSave.emit(this.Shipment);
        }, (err: Response) => {
            this.handleError(err);
        });
    }

    private handleError(err: Response): void {
        let validationErrorDictionary = JSON.parse(err.text());
        for (var fieldName in validationErrorDictionary) {
            if (validationErrorDictionary.hasOwnProperty(fieldName)) {
                this.errors[fieldName] = validationErrorDictionary[fieldName];
            }
        }
    }
}