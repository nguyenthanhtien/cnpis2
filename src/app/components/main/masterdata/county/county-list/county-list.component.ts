import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CountyDetailComponent } from '../county-detail/county-detail.component';
import { CountyQueryResult } from '../countyQueryResult';
import { MessageBox, DataService, NotificationService } from 'src/app/core/services';
import { County, ProvinceMasterData, City } from 'src/app/core/models';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { CountyService } from 'src/app/core/api/masterdata.service';


@Component({
  selector: 'county-list',
  templateUrl: './county-list.component.html',
  providers: [DataService, BsModalService, TranslatePipe, MessageBox]
})

export class CountyListComponent implements OnInit {

  public counties: County[] = [];
  public totalItems: number;
  public pageSize: number;
  public SelectedCounty: County;
  public condition: County = new County();
  public searchCondition: County = new County();
  public searchProvince: ProvinceMasterData = new ProvinceMasterData();
  public searchCityName: City = new City();
  public pageNumber: number;
  public pageList: any;
  public rotate = true;
  private sortColumn: string;
  private isSorted: boolean;
  public showPageForm: number;
  public showPageTo: number;

  constructor(
    private mdlService: BsModalService,
    private msgBox: MessageBox,
    private translatePipe: TranslatePipe,
    private notificationService: NotificationService,
    private coutyService: CountyService) { }

  ngOnInit() {
    this.initFormCounty();
    this.getAllCounty();
  }

  initFormCounty() {
    this.sortColumn = 'CountyName';
    this.isSorted = true;
    this.pageSize = 20;
    this.pageNumber = 1;
    this.showPageForm = 0;
    this.showPageTo = 0;
    this.totalItems = 0;
    this.pageList = [
      {
        value: 20,
        text: 20,
      },
      {
        value: 25,
        text: 25,
      },
      {
        value: 50,
        text: 50,
      },
      {
        value: 100,
        text: 100,
      },
    ];
  }

  public sortcolumnName(columnName: string): void {
    if (this.sortColumn === columnName) {
      this.isSorted = !this.isSorted;
    } else {
      this.sortColumn = columnName;
      this.isSorted = false;
    }
    this.getAllCounty();
  }

  public resetSearch(pageNo: number) {
    setTimeout(() => {
      this.pageNumber = pageNo;
      this.getAllCounty();
    });
  }

  public searchCounty(pageNo: number) {
    this.pageNumber = pageNo;
    const params = {
      'PageSize': this.pageSize,
      'PageNumber': this.pageNumber,
      'OrderBy': this.sortColumn,
      'OrderType': this.isSorted,
      'ProvinceName': this.searchProvince.ProvinceName,
      'CityName': this.searchCityName.CityName,
      'CountyCode': this.searchCondition.CountyCode,
      'CountyName': this.searchCondition.CountyName
    };
    this.getListCounty(params);
  }

  public getAllCounty(): void {
    const params = {
      'PageSize': this.pageSize,
      'PageNumber': this.pageNumber,
      'OrderBy': this.sortColumn,
      'OrderType': this.isSorted
    };
    this.getListCounty(params);
  }

  public getListCounty(params: any) {
    this.coutyService.getAllCounty(params).subscribe((queryResult: CountyQueryResult) => {
      this.totalItems = queryResult.TotalItems;
      this.counties = queryResult.ListData;
      if (this.totalItems === 0) {
        this.showPageForm = 0;
        this.showPageTo = 0;
      } else {
        this.showPageForm = (this.pageNumber - 1) * this.pageSize + 1;
        this.showPageTo = this.showPageForm + queryResult.ListData.length - 1;
      }
    }, err => {
      this.errorMessage('msg-county-list-error-api');
    });
  }

  public pageChanged($event: any): void {
    this.pageNumber = $event.page;
    this.getAllCounty();
  }

  public showFormCreate(): void {
    const mdlRef = this.mdlService.show(CountyDetailComponent, {
      class: 'gray modal-lg', backdrop: 'static', initialState: {}
    });
    mdlRef.content.OnSaveChanged.subscribe(() => {
      this.getAllCounty();
    });
  }

  public showFormEdit(county: any): void {
    this.coutyService.getCountyId(county.CountyId).subscribe((res: any) => {
      this.SelectedCounty = res;
      const mdlRef = this.mdlService.show(CountyDetailComponent, {
        class: 'gray modal-lg', backdrop: 'static', initialState: { county: Object.assign({}, this.SelectedCounty) }
      });
      mdlRef.content.OnSaveChanged.subscribe(() => {
        this.getAllCounty();
      });
    }, () => {
      this.errorMessage('msg-county-id-error-api');
    });
  }

  public deleteItemCounty(county: any): void {
    const mdlRef = this.msgBox.confirm(
      this.translatePipe.transform('msg-county-title-delete', null),
      this.translatePipe.transform('msg-county-active-delete', null)
    );
    mdlRef.content.OnResult.subscribe(() => {
      this.coutyService.deleteCountyById(county.CountyId).subscribe(() => {
        this.successMessage('msg-55-county');
        this.getAllCounty();
      }, error => {
        this.errorProgress(error);
      });
    });
  }

  public selectAll(value: any): void {
    this.counties.forEach((county: County, index: number) => {
      county.IsSelected = value;
    });
  }

  public deleteMultipleItemCounty(): void {
    const lst = this.counties.filter(e => e.IsSelected === true).map(e => e.CityId);
    if (lst.length > 0) {
      const mdlRef = this.msgBox.confirm(
        this.translatePipe.transform('msg-county-title-delete', null),
        this.translatePipe.transform('msg-county-active-delete', null)
      );
      mdlRef.content.OnResult.subscribe(() => {
        this.coutyService.multipleDeleteCounty(lst).subscribe(() => {
          this.successMessage('msg-55-county');
          this.getAllCounty();
        }, err => {
          this.errorProgress(err);
        });
      });
    } else {
      this.errorMessage('msg-city-delete-selete');
    }
  }

  public errorProgress(error: any) {
    if (error) {
      switch (error['_body']) {
        case '"CountyId, 113"': {
          this.errorMessage('msg-6-county');
          break;
        }
        default:
          this.errorMessage('msg-56-county');
          break;
      }
    }
  }

  public errorMessage(message: any) {
    this.notificationService.printErrorMessage(
      this.translatePipe.transform(message, null));
  }

  public successMessage(message: any) {
    this.notificationService.printSuccessMessage(
      this.translatePipe.transform(message, null));
  }
}
