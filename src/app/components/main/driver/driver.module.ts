import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverComponent } from './driver.component';
import { UploadprofileComponent } from './uploadprofile/uploadprofile.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, ModalModule, AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './driver.routes';
import { ProgressHttpModule } from 'angular-progress-http';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { TranslateModule } from 'src/app/core/pipes/translate.module';
import { DriverService } from 'src/app/core/api';
import { DataService, UploadService, UtilityService, NotificationService, MessageBox } from 'src/app/core/services';
/////
import { BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    ProgressHttpModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forChild(mainRoutes),
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [
    UploadprofileComponent,
    DriverComponent,
  ],
  providers: [
    DataService,
    DriverService,
    UploadService,
    UtilityService,
    NotificationService,
    MessageBox,
    TranslatePipe
  ]
})
export class DriverModule { }
