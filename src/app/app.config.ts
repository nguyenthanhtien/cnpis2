export class AppConfig {
    public static BASE_API = 'http://10.88.14.192/CNPIS_BE';
    public static CURRENT_USER = 'CURRENT_USER';
    public static APP_LANGUAGE = 'CNPIS_LANGUAGE';
    public static CURRENT_ROLE = 'CURRENT_ROLE';
    public static CT_REMOTE_USER = 'CT_REMOTE_USER';
    public static AUTHENTICATION_API = 'http://10.88.114.29/eam/User/';
    public static ACTIVE_AUTHORIZATION = false;
}