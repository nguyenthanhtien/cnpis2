import {Injectable} from '@angular/core';
import { TRANSLATIONS } from '../language/translations';

@Injectable()
export class TranslateService {
  private _currentLang: string;

  public get currentLang() {
    return this._currentLang;
  }

  constructor() {
  }

  public use(lang: string): void {
    this._currentLang = lang;
  }

  private translate(key: string): string {
    let translation = key;

    if (TRANSLATIONS[this.currentLang] && TRANSLATIONS[this.currentLang][key]) {
      return TRANSLATIONS[this.currentLang][key];
    }

    return translation;
  }

  public instant(key: string) {
    return this.translate(key);
  }
}