import { Routes } from '@angular/router';
//import { ConfigurationsettingComponent } from './configurationsetting.component';
import { EdimessagescheduleComponent } from './edimessageschedule/edimessageschedule.component';
import { IpaddressmanagementComponent } from './ipaddressmanagement/ipaddressmanagement.component';
import { IntergrationconfigurationComponent } from './intergrationconfiguration/intergrationconfiguration.component';
import { ArchiveconfigurationComponent } from './archiveconfiguration/archiveconfiguration.component';
import { NotificationconfigurationComponent } from './notificationconfiguration/notificationconfiguration.component';
import { AUTHGROUP } from 'src/app/core/common';
import { RoleGuardService } from 'src/app/core/services';


export const mainRoutes: Routes = [

    { path: 'edimessageschedule', component: EdimessagescheduleComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'ipaddressmanagement', component: IpaddressmanagementComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'intergrationconfiguration', component: IntergrationconfigurationComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'archiveconfiguration', component: ArchiveconfigurationComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'notificationconfiguration', component: NotificationconfigurationComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } }
]
