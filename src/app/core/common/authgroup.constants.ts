export class AUTHGROUP {
    public static SYSTEM_ADMIN = 'SYSTEMADMIN';
    public static DISTRICT_ADMIN = 'DISTRICTADMIN';
    public static HUB_CENTER_USER = 'HUBCENTERUSER';
    public static IT_APP_SUPPORT = 'ITAPPSUPPORT';
}