import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../../core/services/translate.service';
import { AuthService, UtilityService } from 'src/app/core/services';
import { AppConfig } from '../../app.config';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers : [TranslatePipe]

})
export class HeaderComponent implements OnInit {

  languageList: any[];
  languageName: string;
  constructor(private translator: TranslateService,
  private authService : AuthService,
  private utilityService : UtilityService) { }

  ngOnInit() {
    this.languageName =  this.getLanguage();
    this.translator.use(this.languageName);
    this.languageList = [];
    this.languageList.push({ label: 'English', value: 'en' });
    this.languageList.push({ label: '中文', value: 'cn' });
  }
  getLanguage() : any {
    if(localStorage.getItem(AppConfig.APP_LANGUAGE) == null){
      this.setLanguage('en');
    }
    return localStorage.getItem(AppConfig.APP_LANGUAGE);
  }
  
  selectedLanguage(currentValue : string) : boolean {
    return localStorage.getItem(AppConfig.APP_LANGUAGE) === currentValue;
  }
  setLanguage(valueLang: any) {
    this.translator.use(valueLang);
    localStorage.setItem(AppConfig.APP_LANGUAGE, valueLang);
  }
  logout(){
    if(localStorage.getItem(AppConfig.CURRENT_USER)){
      this.authService.logout();
      this.utilityService.navigateToLogin();
    }
  }
}
