import { Routes } from '@angular/router';
import { DriverComponent } from './driver.component';
import { UploadprofileComponent } from './uploadprofile/uploadprofile.component';
import { AuthGuardService } from 'src/app/core/services/guard.service';
import { AUTHGROUP } from 'src/app/core/common';
import { RoleGuardService } from 'src/app/core/services';

export const mainRoutes: Routes = [

    { path: '', component: DriverComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'upload-profile', component: UploadprofileComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.HUB_CENTER_USER, AUTHGROUP.SYSTEM_ADMIN] } }

]