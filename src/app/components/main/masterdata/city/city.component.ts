import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageBox, DataService, NotificationService } from 'src/app/core/services';
import { City, CityQueryResult, ProvinceMasterData } from 'src/app/core/models';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { CityService, ProvinceService } from '../../../../core/api/masterdata.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  providers: [DataService, BsModalService, TranslatePipe, MessageBox]
})
export class CityComponent implements OnInit {
  @ViewChild('modalAdd') modalAdd: ModalDirective;
  @ViewChild('modalEdit') modalEdit: ModalDirective;

  public cities: City[] = [];
  public searchCondition: City = new City();
  public provinceMasterData: ProvinceMasterData[] = [];
  public searchProvince: ProvinceMasterData = new ProvinceMasterData();
  public totalItems: number;
  public pageSize: number;
  public selectedCity: City;
  public pageList: any;
  public sortColumn: string;
  public isSorted: boolean;
  public pageNumber: number;
  public rotate: true;
  public registerForm: FormGroup;
  public submitted: boolean;
  public showPageForm: number;
  public showPageTo: number;

  constructor(
    private msgBox: MessageBox,
    private cityService: CityService,
    private notificationService: NotificationService,
    private provinceService: ProvinceService,
    private pipeTranslate: TranslatePipe,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initFormCiy();
    this.getListCity();
    this.createForm();
  }

  public initFormCiy() {
    this.sortColumn = 'CityName';
    this.pageSize = 20;
    this.isSorted = true;
    this.pageNumber = 1;
    this.submitted = false;
    this.showPageTo = 0;
    this.showPageForm = 0;
    this.totalItems = 0;
    this.pageList = [
      {
        value: 20,
        text: 20
      },
      {
        value: 25,
        text: 25
      },
      {
        value: 50,
        text: 50
      },
      {
        value: 100,
        text: 100
      }
    ];
  }

  get cityFrm() {
    return this.registerForm.controls;
  }

  public sortColumnName(columnName: string): void {
    if (this.sortColumn === columnName) {
      this.isSorted = !this.isSorted;
    } else {
      this.sortColumn = columnName;
      this.isSorted = false;
    }
    this.getListCity();
  }

  public resetSearch(pageNo: number) {
    setTimeout(() => {
      this.pageNumber = pageNo;
      this.getListCity();
    });
  }

  public searchListCity(pageNo: number) {
    this.pageNumber = pageNo;
    const params = {
      PageSize: this.pageSize,
      PageNumber: this.pageNumber,
      OrderBy: this.sortColumn,
      OrderType: this.isSorted,
      ProvinceName: this.searchProvince.ProvinceName,
      CityCode: this.searchCondition.CityCode,
      CityName: this.searchCondition.CityName
    };
    this.getAllListCity(params);
  }

  public getListCity(): void {
    const params = {
      PageSize: this.pageSize,
      PageNumber: this.pageNumber,
      OrderBy: this.sortColumn,
      OrderType: this.isSorted,
    };
    this.getAllListCity(params);
  }

  public getAllListCity(params: any) {
    this.cityService.getAllCity(params).subscribe(
      (queryResult: CityQueryResult) => {
        this.totalItems = queryResult.TotalItems;
        if (this.totalItems === 0) {
          this.showPageForm = 0;
          this.showPageTo = 0;
        } else {
          this.showPageForm = (this.pageNumber - 1) * this.pageSize + 1;
          this.showPageTo = this.showPageForm + queryResult.ListData.length - 1;
        }
        this.cities = queryResult.ListData;
      }, () => {
        this.errorMessage('msg-city-list-error-api');
      }
    );
  }

  public createForm() {
    this.registerForm = this.formBuilder.group({
      ProvinceId: [''],
      CityId: [''],
      ProvinceName: ['', [Validators.required, Validators.maxLength(255)]],
      ProvinceCode: ['', [Validators.required, Validators.maxLength(32)]],
      CityName: ['', [Validators.required, Validators.maxLength(255)]],
      CityCode: ['', [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(32)]]
    });
  }

  public pageChanged(number: any): void {
    this.pageNumber = number;
    this.getListCity();
  }

  public getAllProvince(): void {
    this.cityService.getAllProvince().subscribe(
      (response: any) => {
        this.provinceMasterData = response;
      }, () => {
        this.errorMessage('msg-city-province-error-api');
      }
    );
  }

  public getAllCityById(id: any) {
    this.cityService.getCityById(id).subscribe((response: any) => {
      this.cityFrm.CityName.setValue(response.CityName);
      this.cityFrm.CityCode.setValue(response.CityCode);
      this.cityFrm.CityId.setValue(response.CityId);
      this.provinceService
        .getDetailProvince(response.ProvinceId)
        .subscribe((res: any) => {
          this.cityFrm.ProvinceName.setValue(res.ProvinceName);
          this.cityFrm.ProvinceCode.setValue(res.ProvinceCode);
          this.cityFrm.ProvinceId.setValue(res.ProvinceId);
        }, () => {
          this.errorMessage('msg-city-error-api-city-by-id');
        });
    }, () => {
      this.errorMessage('msg-city-error-api-province-by-id');
    });
  }

  public onChangeProvinceCode($event) {
    if (_.includes(this.provinceMasterData, $event.item)) {
      this.cityFrm.ProvinceCode.setValue($event.item.ProvinceCode);
      this.cityFrm.ProvinceId.setValue($event.item.ProvinceId);
    } else {
      this.cityFrm.ProvinceName.setValue('');
    }
  }

  public selectAll(value: any): void {
    this.cities.forEach((city: City, index: number) => {
      city.IsSelected = value;
    });
  }

  public showFormCreate() {
    this.modalAdd.show();
    this.getAllProvince();
  }

  public showFormEdit(id: any) {
    this.getAllCityById(id);
    this.getAllProvince();
    this.modalEdit.show();
  }

  public deleteItemCity(id: any) {
    const mdlRef = this.msgBox.confirm(
      this.pipeTranslate.transform('msg-city-delete', null),
      this.pipeTranslate.transform('msg-city-confirm-delete', null)
    );
    mdlRef.content.OnResult.subscribe(() => {
      this.cityService.deleteCityById(id).subscribe(() => {
        this.successMessage('msg-55-county');
        this.getListCity();
      }, error => {
        this.errorAllOfCity(error, 'msg-56-county');
      });
    });
  }

  public deleteMultipleItemCity() {
    const lst = this.cities.filter(e => e.IsSelected === true).map(e => e.CityId);
    if (lst.length > 0) {
      const mdlRef = this.msgBox.confirm(
        this.pipeTranslate.transform('msg-city-delete', null),
        this.pipeTranslate.transform('msg-city-confirm-delete', null)
      );
      mdlRef.content.OnResult.subscribe(() => {
        this.cityService.multipleDeleteCity(lst).subscribe(() => {
          this.successMessage('msg-49-city');
          this.getListCity();
        }, error => {
          this.errorAllOfCity(error, 'msg-50-city');
        });

      });
    } else {
      this.errorMessage('msg-city-delete-selete');
    }
  }

  public onSaveChangeCity() {
    this.submitted = true;
    if (this.registerForm.valid === true) {
      const obj = this.registerForm.getRawValue();
      if (!obj.CityId) {
        this.cityService.createCity(obj).subscribe(
          res => {
            this.successMessage('msg-51-county');
            this.onResetCity();
            this.getListCity();
          },
          err => {
            this.errorAllOfCity(err, 'msg-52-county');
          }
        );
      } else {
        const mdlRef = this.msgBox.confirm(
          this.pipeTranslate.transform('msg-city-record', null),
          this.pipeTranslate.transform('msg-city-confirm-update', null)
        );
        mdlRef.content.OnResult.subscribe(() => {
          this.cityService.updateCity(obj).subscribe(
            res => {
              this.successMessage('msg-53-county');
              this.onReSetEditCity();
              this.getListCity();
            },
            err => {
              this.errorAllOfCity(err, 'msg-54-county');
            }
          );
        });
      }
    }
  }

  public errorAllOfCity(error: any, errMsg: any) {
    if (error) {
      switch (error['_body']) {
        case '"CityCode, 1111"': {
          this.errorMessage('msg-6-county');
          break;
        }
        case '"CityID, 113"': {
          this.errorMessage('msg-56-county');
          break;
        }
        default: {
          this.errorMessage(errMsg);
          break;
        }
      }
    }
  }

  public errorMessage(message: any) {
    this.notificationService.printErrorMessage(
      this.pipeTranslate.transform(message, null)
    );
  }

  public successMessage(message: any) {
    this.notificationService.printSuccessMessage(
      this.pipeTranslate.transform(message, null)
    );
  }

  public onResetCity() {
    this.submitted = false;
    this.registerForm.reset();
    this.modalAdd.hide();
  }

  public onReSetEditCity() {
    this.submitted = false;
    this.registerForm.reset();
    this.modalEdit.hide();
  }
}
