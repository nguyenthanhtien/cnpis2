import { FormControl, Validators } from '@angular/forms';
import { CommonRegexp } from '../../../core/validations/commonregexp';

export class ShipmentCustomValidators extends Validators {
    static isTrackingNumber(control: FormControl) {
        if (control.value && control.value.length > 0) {
            return (control.value.length != 11 && control.value.length != 18) ? { invalidTrackingNumber: true } : null;
        } else {
            return null;
        }
    }

    static validDateTime(control: FormControl) {
        if (control.value && control.value.length > 0) {
            const matches = control.value.match(CommonRegexp.DATE_TIME_FORMAT);
            return (matches == null || matches.length == 0) ? { validDateTime: true } : null;
        } else {
            return null;
        }
    }

    static validPackageType(control: FormControl) {
        if (control.value && control.value.length > 0) {
            let result = control.value != 'Doc' && control.value != 'Non-Doc' && control.value != 'Others';
            return (result ? { invalidPackageType : result } : null);
        } else {
            return null;
        }
    }
}