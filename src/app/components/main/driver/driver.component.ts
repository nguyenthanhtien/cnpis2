import { Component, OnInit, ViewChild } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Driver, DriverQueryResult, SearchDriver, DriverCenterMasterData } from 'src/app/core/models';
import { NotificationService, MessageBox } from 'src/app/core/services';
import { DriverService } from 'src/app/core/api';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';

@Component({
    selector: 'app-driver',
    templateUrl: './driver.component.html'
})
export class DriverComponent implements OnInit {
    @ViewChild('modalAdd') modalAdd: ModalDirective;
    @ViewChild('modalEdit') modalEdit: ModalDirective;

    formDriver: FormGroup;
    formSearch: FormGroup;
    public drivers: Driver[] = [];
    public upsFacilitys: DriverCenterMasterData[] = [];
    public totalItems: number;
    public pageSize: number;
    public pageNumber: number;
    public pageDetail: number;
    public itemTo: number;
    public itemFrom: number;
    public searchCondition: SearchDriver;
    public sortColumn: string = '';
    public DriverId: string;
    public driver: Driver = new Driver();
    public isSorted: boolean = true;
    public pageList: any;
    submitted = false;
    requestSearch: any;
    constructor(
        private formBuilder: FormBuilder,
        private driverService: DriverService,
        private notificationService: NotificationService,
        private msgBox: MessageBox,
        private pipeTranslate: TranslatePipe

    ) { }
    ngOnInit(): void {
        this.initDriverComponent();
        this.searchForm();
        this.createForm();
        this.getUPSFacilityMasterData();
        this.search();
    }
    initDriverComponent() {
        this.sortColumn = 'CreatedTime';
        this.isSorted = false;
        this.pageSize = 20; 
        this.pageNumber = 1;
        this.pageList = [
            {
                value: 20,
                text: 20,
            },
            {
                value: 30,
                text: 30,
            },
            {
                value: 50,
                text: 50,
            },
            {
                value: 100,
                text: 100,
            }
        ];
    }
    resetSearch() {
        this.formSearch.reset();
        this.pageNumber = 1;
        this.search();
    }
    resetForm() {
        this.submitted = false;
        this.modalAdd.hide();
        this.formDriver.reset();
        this.search();
    }
    resetEditForm() {
        this.modalEdit.hide();
        this.search();
    }
    get f() { return this.formDriver.controls; }

    createForm(): void { 
        this.formDriver = this.formBuilder.group({
            DriverName: [undefined, [Validators.required, Validators.maxLength(32)]],
            DriverPhoneNumber: [undefined, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(32)]],
            DriverNationIdType: ['01', []],
            DriverNationId: [undefined, [Validators.required, Validators.maxLength(30)]],
            DriverUPSEmpId: [undefined, [Validators.required, Validators.maxLength(30)]],
            DriverType: [undefined, Validators.required],
            DriverCompany: [undefined, [Validators.maxLength(30)]],
            DriverStatus: [1, [Validators.required]],
            UPSFacilityId: [undefined],
            UPSFacilityName: [undefined, [Validators.required, Validators.maxLength(255)]],
            UPSFacilitySLIC: [undefined, [Validators.required,Validators.maxLength(255)]],
            Street: [undefined],
            PostalCode: [undefined],
            DeviceId: [undefined, [Validators.maxLength(255),Validators.pattern('^[0-9a-zA-Z]*$')]],
            Remarks: [undefined, [Validators.maxLength(255), Validators.pattern('^[0-9a-zA-Z]*$')]]
        });
    }

    searchForm(): void {
        this.formSearch = this.formBuilder.group({
            Name: [undefined, [Validators.maxLength(255)]],
            PhoneNumber: [undefined, [Validators.maxLength(255)]],
            NationId: [undefined, [Validators.maxLength(255)]],
            UPSEmpId: [undefined, [Validators.maxLength(255)]],
            Type: [''],
            Company: [undefined, [Validators.maxLength(255)]],
            Status: [''],
            UPSFacility: [undefined, [ Validators.maxLength(255)]],
            SLIC: [undefined, [Validators.maxLength(255)]]
        });
    } 
    public sort(columnName: string): void {
        if (this.sortColumn == columnName) {
            this.isSorted = !this.isSorted;
        } else {
            this.sortColumn = columnName;
            this.isSorted = false;
        }
        this.search();
    }
    public pageChanged($event: any): void {
        this.pageNumber = $event.page;
        this.search();
    }
    public onPageSizeChange(value: any) {
        this.pageSize = value;
        this.pageNumber = 1;
        this.search();
    }
    showAddModal() {
        this.submitted = false;
        this.createForm();
        this.modalAdd.config = { backdrop: 'static' };
        this.modalAdd.show();
    }
    showEditModal(id: any) {
        this.DriverId = id;
        this.loadDriverDetail(id);
        this.modalEdit.show();
    }
    notificationError(msg: string) {
        this.notificationService.printErrorMessage(this.pipeTranslate.transform(msg, null));
    }
    notificationSuccess(msg: string) {
        this.notificationService.printSuccessMessage(this.pipeTranslate.transform(msg, null));
    }
    public getUPSFacilityMasterData(): void {
        this.driverService.getAllUPSFacilityMasterData().subscribe((response: DriverCenterMasterData[]) => {
            this.upsFacilitys = response;
        }, (err: any) => { });
    }
    search() {
        this.searchCondition = this.formSearch.value;
        let params = {
            'PageSize': this.pageSize, 'PageNumber': this.pageNumber,
            'OrderBy': this.sortColumn,
            'OrderType': this.isSorted,
            'DriverName': this.searchCondition.Name,
            'UPSFacilityName': this.searchCondition.UPSFacility,
            'UPSFacilitySLIC': this.searchCondition.SLIC,
            'DriverType': this.searchCondition.Type,
            'DriverNationId': this.searchCondition.NationId,
            'DriverPhoneNumber': this.searchCondition.PhoneNumber,
            'DriverUPSEmpId': this.searchCondition.UPSEmpId,
            'DriverCompany': this.searchCondition.Company,
            'DriverStatus': this.searchCondition.Status
        };
        this.driverService.getAllDriver(params)
            .subscribe((res: DriverQueryResult) => {
                this.totalItems = res.TotalItems;
                this.drivers = res.ListData;
                if (this.totalItems == 0) {  
                    this.itemFrom = 0; this.itemTo = 0; } 
                else {
                    this.itemFrom = (this.pageNumber - 1) * this.pageSize + 1;
                    this.itemTo = this.itemFrom + res.ListData.length - 1;
                }
            }, (err) => {
                this.notificationError('lb-not-data');
            });
    } 
    upsFacilityChanged(id) {
        if (id) {
            if (this.requestSearch) {
                this.requestSearch.unsubscribe();
            }
            this.requestSearch = this.driverService.getUPSFacilityMasterDataById(id).subscribe((res: any) => {
                this.formDriver.patchValue({
                    Street: res.Street,
                    PostalCode: res.PostalCode,
                    UPSFacilitySLIC: res.UPSFacilitySLIC
                });
            }, (error: any) => { });
        }
    }
    loadDriverDetail(id: any) {
        this.driverService.getDriverDetail(id)
            .subscribe((response: any) => {
                this.formDriver.patchValue({
                    DriverName: response.DriverName,
                    DriverPhoneNumber: response.DriverPhoneNumber,
                    DriverNationIdType: response.DriverNationIdType,
                    DriverNationId: response.DriverNationId,
                    DriverUPSEmpId: response.DriverUPSEmpId,
                    DriverType: response.DriverType,
                    DriverCompany: response.DriverCompany,
                    UPSFacilityId: response.UPSFacilityId,
                    DriverStatus: response.DriverStatus,
                    UPSFacilityName: _.find(this.upsFacilitys, e => e['UPSFacilityId'] == response.UPSFacilityId).UPSFacilityName,
                    UPSFacilitySLIC: _.find(this.upsFacilitys, e => e['UPSFacilityId'] == response.UPSFacilityId).UPSFacilitySLIC,
                    Street: _.find(this.upsFacilitys, e => e['UPSFacilityId'] == response.UPSFacilityId).Street,
                    PostalCode: _.find(this.upsFacilitys, e => e['UPSFacilityId'] == response.UPSFacilityId).PostalCode,
                    CreatedBy: response.CreatedBy,
                    CreatedTime: response.CreatedTime,
                    UpdatedBy: response.UpdatedBy,
                    UpdatedTime: response.UpdatedTime,
                    DeviceId: response.DeviceId,
                    Remarks: response.Remarks
                });
            });
    }
    public onChangeUPSFacility($event) {
        if (_.includes(this.upsFacilitys, $event.item)) {
          this.f.Street.setValue($event.item.Street);
          this.f.PostalCode.setValue($event.item.PostalCode);
          this.f.UPSFacilityId.setValue($event.item.UPSFacilityId);
        } else {
          this.f.UPSFacilityName.setValue('');
        }
    }
    onSelectDriver(name, $event) {
        this.driver[name] = $event.item[name];
    }
    onSelectDriverUpdateDropdownValue(name, $event) {
        this.onSelectDriver(name, $event);
        let id = $event['item'] ? $event['item']['UPSFacilityId'] : $event;
        if (id) {
            if (this.requestSearch) {
                this.requestSearch.unsubscribe();
            }
            this.requestSearch = this.driverService.getUPSFacilityMasterDataById(id).subscribe((res: any) => {
                this.formDriver.patchValue({
                    Street: res.Street,
                    PostalCode: res.PostalCode,
                    UPSFacilitySLIC: res.UPSFacilitySLIC,
                    UPSFacilityName: res.UPSFacilityName
                });
            }, (error: any) => { });
        }
    }
    createDriver(): void {
        this.submitted = true;
        let oldDriver = this.driver;
        this.driver = this.formDriver.value;
        // Set again from old value 
        this.driver['UPSFacilityId'] = oldDriver['UPSFacilityId'];
        if (this.formDriver.invalid) {
            this.notificationError('invalid-formDriver');
            return;
        }
        else {
            if (this.driver.DriverId == undefined) {
                this.driver.CreatedTime = new Date(new DatePipe('en-US').transform(Date.now(), 'yyyy/MM/dd hh:mm:ss'));
                this.driverService.postDriver(this.driver)
                    .subscribe((response: any) => {
                        this.modalAdd.hide();
                        this.formDriver.reset();
                        this.search();
                        this.notificationSuccess('msg-19');
                    }, error => {
                        if (error._body.indexOf('DriverMobilePhoneNumber') != -1) {
                            this.notificationError('msg-6-Driver-MobilePhone-Number');
                        }
                        else if (error._body.indexOf('DriverNationId') != -1) {
                            this.notificationError('msg-6-Driver-NationId');
                        }
                        else if (error._body.indexOf('DRIVERUPSEMPID') != -1) {
                            this.notificationError('msg-6-Employee-ID-driver');
                        }
                        else {
                            this.notificationError('msg-20');
                        }
                    });

            }
        }
    }
    editDriver() {
        this.submitted = true;
        this.driver = this.formDriver.value;
        this.driver.DriverId = this.DriverId;
        this.driver.UpdatedTime = new Date(new DatePipe('en-US').transform(Date.now(), 'yyyy/MM/dd hh:mm:ss'));
        if (this.formDriver.invalid) {
            this.notificationError('invalid-formDriver');
            return;
        }
        else {
            this.driverService.putDriver(this.driver)
                .subscribe(() => {
                    this.modalEdit.hide();
                    this.formDriver.reset();
                    this.search();
                    this.notificationSuccess('msg-21');
                },
                error => {
                    this.notificationError('msg-22');
                });
            }

    }
    statusDriver(driver: Driver) {
        if (driver.DriverStatus == 1) {
            let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-Deactivate-driver', null), this.pipeTranslate.transform('msg-confirm-Deactivate-driver', null));
            mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
                driver.DriverStatus = 0;
                this.driverService.putStatusDriver(driver.DriverId)
                    .subscribe(() => {
                        this.notificationSuccess('msg-23');
                    }, (error) => {
                        this.notificationError('msg-24');
                    });
            }, () => {
                this.notificationError('msg-0');
            });
        }
        else {
            let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-Activate-driver', null), this.pipeTranslate.transform('msg-confirm-Activate-driver', null));
            mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
                driver.DriverStatus = 1;
                this.driverService.putStatusDriver(driver.DriverId)
                    .subscribe(() => {
                        this.notificationSuccess('msg-25');
                    }, (error) => {
                        this.notificationError('msg-26');
                    });
            }, (error) => {
                this.notificationError('msg-0');
            });
        }
    }
}


