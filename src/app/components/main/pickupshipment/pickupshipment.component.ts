import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService, MessageBox, NotificationService } from 'src/app/core/services';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { PickupShipmentService } from 'src/app/core/api';
import { PickupShipment, PickupShipmentQueryResult } from 'src/app/core/models';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-pickupshipment',
  templateUrl: './pickupshipment.component.html',
  styleUrls: ['../user/user.component.css'],
  providers: [BsModalService, DataService, MessageBox, TranslatePipe, PickupShipmentService]
})
export class PickupshipmentComponent implements OnInit {

  @ViewChild('modalEdit') public modalEdit: ModalDirective;

  fromInput: boolean = false;
  toInput: boolean = false;

  public selectChoose: boolean = false;
  public conditionPickupShipment: PickupShipment = new PickupShipment();
  public totalItems: number;
  public pageSize: number;
  public pageNumber: number;
  public sortColumn: string;
  public isSorted: boolean;
  public pageList: any;
  public itemTo: number;
  public itemFrom: number;
  isFormValid : boolean = true;
  public listItem: PickupShipment;
  public listPickupShipment: PickupShipment[];
  public formPickup: FormGroup;

  public formatDateFrom: Date = new Date();
  public formatDateTo: Date = new Date();

  public resetSearchPickupShipment: PickupShipment = new PickupShipment();
  public searchPickupShipment: PickupShipment = new PickupShipment();

  settingsFrom = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy/MM/dd hh:mm:ss',
    defaultOpen: true,
    closeOnSelect: true
  }
  settingsTo = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy/MM/dd hh:mm:ss',
    defaultOpen: true,
    closeOnSelect: true
  }

  constructor(
    public dataService: DataService,
    public pickupshipmentService: PickupShipmentService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private pipeTranslate: TranslatePipe) { }

  ngOnInit() {
    this.initPickup();
    this.search();
    this.createFrom();
  }
  initPickup() {
    this.pageSize = 20;
    this.pageNumber = 1;
    this.sortColumn = 'PickUpDateTime';
    this.isSorted = false;
    this.pageList = [
      {
        value: 20,
        text: 20,
      },
      {
        value: 25,
        text: 25,
      },
      {
        value: 50,
        text: 50,
      },
      {
        value: 100,
        text: 100,
      },
    ];
  }

  fromInputValue(value: boolean) {
    this.fromInput = value;
  }

  toInputValue(value: boolean) {
    this.toInput = value;
  }

  createFrom(): void {
    this.formPickup = this.formBuilder.group({
      TrackingNo: [null, [Validators.required, Validators.max(255)]],
      DriverUPSEmpId: [null, [Validators.required, Validators.maxLength(255)]],
      DriverNationalId: [null, [Validators.required, Validators.maxLength(255)]],
      ShipperUPSAccNum: [null, [Validators.maxLength(255)]],
      From: [null, [Validators.required]],
      To: [null, [Validators.required]],
      PackageType: ["", Validators.required],
      ConsigneePhoneNumber: [null, [Validators.required, Validators.maxLength(255)]]
    });
  }

  public sort(columnName: string): void {
    if (this.sortColumn == columnName) {
      this.isSorted = !this.isSorted;
    } else {
      this.sortColumn = columnName;
      this.isSorted = false;
    }
    this.search();
  }

  search(): void {
    let params = {
      'PageSize': this.pageSize,
      'PageNumber': this.pageNumber,
      'OrderBy': this.sortColumn,
      'OrderType': this.isSorted,
      'TrackingNo': this.searchPickupShipment.TrackingNo,
      'DriverEmployeeId': this.searchPickupShipment.DriverUPSEmpId,
      'DriverNationalId': this.searchPickupShipment.DriverNationalId,
      'ShipperUPSAccNum': this.searchPickupShipment.ShipperUPSAccNum,
      'PackageType': this.searchPickupShipment.PackageType,
      'ConsigneePhoneNumber': this.searchPickupShipment.ConsigneePhoneNumber,
      'From': this.searchPickupShipment.From,
      'To': this.searchPickupShipment.To
    };
    this.pickupshipmentService.getAllPickupShipment(params).subscribe(
      (queryResult: PickupShipmentQueryResult) => {
        this.totalItems = queryResult.TotalItems;
        this.listPickupShipment = queryResult.ListData;
        if (this.totalItems == 0) {
          this.itemFrom = 0; this.itemTo = 0;
        }
        else {
          this.itemFrom = (this.pageNumber - 1) * this.pageSize + 1;
          this.itemTo = this.itemFrom + queryResult.ListData.length - 1;
        }
      }, (err) => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-not-fount', null)));
  }

  public pageChanged($event: any): void {
    this.pageNumber = $event.page;
    this.search();
  }

  public resetSearch(value : any): void {
    this.searchPickupShipment = this.resetSearchPickupShipment;
    this.fromInputValue(false);
    this.toInputValue(false);
    this.selectChoose = value;
    this.pageNumber = 1;
    this.search();
  }

  public searchItem(): void {
    this.pageNumber = 1;
    this.conditionPickupShipment = this.formPickup.value;
    this.conditionPickupShipment.From = new DatePipe('en-US').transform(this.conditionPickupShipment.From, 'yyyy/MM/dd HH:mm:ss');
    this.conditionPickupShipment.To = new DatePipe('en-US').transform(this.conditionPickupShipment.To, 'yyyy/MM/dd HH:mm:ss');
    if (this.conditionPickupShipment.From == null || this.conditionPickupShipment.To == null || this.conditionPickupShipment.From <= this.conditionPickupShipment.To) {
      this.searchPickupShipment = this.conditionPickupShipment;
      this.search();
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-wrong-format', null));
    }
  }
}
