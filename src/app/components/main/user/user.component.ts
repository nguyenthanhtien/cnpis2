import { BsModalService } from 'ngx-bootstrap/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { DataService, MessageBox, NotificationService } from 'src/app/core/services';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { UserService } from 'src/app/core/api';
import { User, UserQueryResult, DriverCenterMasterData, FacilityList } from 'src/app/core/models';
import { CommonRegexp } from 'src/app/core/validations/commonregexp';
import { GroupUser } from '../../../core/models/GroupUser';
import { DualListBoxComponent, IItemsMovedEvent } from '../../../../../node_modules/ng2-dual-list-box';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [BsModalService, DataService, MessageBox, TranslatePipe, UserService]
})
export class UserComponent implements OnInit {
  @ViewChild('modalAdd') public modalAdd: ModalDirective;
  @ViewChild('modalEdit') public modalEdit: ModalDirective;

  formUserSearch: FormGroup;
  formUser: FormGroup;
  submitted = false;

  fromInput: boolean = false;
  toInput: boolean = false;

  public UserId: string;
  public pageNumber: number;
  public orderBy: string;
  public orderType: boolean;

  public listItem: User;
  public listFacility: FacilityList;
  public duallistbox: DualListBoxComponent;
  public itemMoveEvent: IItemsMovedEvent;
  public conditionUser: User = new User();
  public pageSize: number = 20;
  public resetSearchUser: User = new User();
  public regexpUser: boolean;
  public searchUser: User = new User();
  public totalItems: number;
  public pageList: any;
  public Users: User[] = [];
  public GroupUsers: GroupUser[] = [];
  public Facilities: DriverCenterMasterData[] = [];
  public userSelect: any[] = [];

  public formatDateFrom: Date = new Date();
  public formatDateTo: Date = new Date();

  public itemTo: number;
  public itemFrom: number;

  isCitiesControlVisible = true;


  settingsFrom = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy/MM/dd HH:mm:ss',
    defaultOpen: true,
    closeOnSelect: true
  }
  settingsTo = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy/MM/dd HH:mm:ss',
    defaultOpen: true,
    closeOnSelect: true
  }

  today: number = Date.now();

  constructor(
    protected formBuilder: FormBuilder,
    private msgBox: MessageBox,
    private notificationService: NotificationService,
    private pipeTranslate: TranslatePipe,
    public dataService: DataService,
    public userService: UserService,
  ) { }

  ngOnInit() {
    this.initUser();
    this.searchForm();
    this.detailForm();
    this.getAll();
    this.getGroupUser();
    this.getFacility();
  }

  initUser() {
    this.pageSize = 20;
    this.pageNumber = 1;
    this.orderBy = "CreateTime"
    this.orderType = false;
    this.pageList = [
      {
        value: 20,
        text: 20,
      },
      {
        value: 25,
        text: 25,
      },
      {
        value: 50,
        text: 50,
      },
      {
        value: 100,
        text: 100,
      },
    ];
  }

  fromInputValue(value: boolean) {
    this.fromInput = value;
  }

  toInputValue(value: boolean) {
    this.toInput = value;
  }

  toggleCitiesControl() {
    this.isCitiesControlVisible = !this.isCitiesControlVisible;
  }

  searchForm(): void {
    this.formUserSearch = this.formBuilder.group({
      ADID: [null, [Validators.required, Validators.maxLength(255)]],
      FullName: [null, [Validators.required, Validators.maxLength(255)]],
      EmailAddress: [null, [Validators.required, Validators.maxLength(255)]],
      GroupUserId: ["", [Validators.required, Validators.maxLength(255)]],
      Facility: [],
      From: [null],
      To: [null],
      Status: [null]
    });
  }

  checkValue(event) {
    return (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode >= 48 && event.charCode <= 57) || event.charCode <= 31;
  }

  get e() { return this.formUser.controls; }

  resetForm() {
    this.submitted = false;
    this.modalAdd.hide();
    this.modalEdit.hide();
    this.formUser.reset();
  }

  detailForm(): void {
    this.formUser = this.formBuilder.group({
      ADID: [null, [Validators.required, Validators.maxLength(7), Validators.pattern('^[0-9a-zA-Z]*$')]],
      FullName: [null, [Validators.required, Validators.maxLength(255)]],
      EmailAddress: [null, [Validators.required, Validators.maxLength(255), Validators.email]],
      GroupUserId: [null, [Validators.required]],
      Selected: [null, [Validators.required]],
    });
  }

  public getGroupUser(): void {
    this.userService.getAllGroupUser().subscribe((response: any) => {
      this.GroupUsers = response;
    });
  }

  public getFacility(): void {
    this.userService.getAllFacility().subscribe((response: any) => {
      this.Facilities = response;
    });
  }

  public getAll(): void {
    let params = {
      'PageSize': this.pageSize, 'PageNumber': this.pageNumber,
      'OrderBy': this.orderBy, 'OrderType': this.orderType,
      'FullName': this.searchUser.FullName, 'Facility': this.searchUser.Facility,
      'Email': this.searchUser.EmailAddress, 'ADID': this.searchUser.ADID, 'Status': this.searchUser.Status,
      'Role': this.searchUser.GroupUserId,
      'From': this.searchUser.From, 'To': this.searchUser.To,
    };
    this.userService.getAllUser(params)
      .subscribe((res: UserQueryResult) => {
        this.totalItems = res.TotalItems;
        this.Users = res.ListData;
        if (this.totalItems == 0) {
          this.itemFrom = 0; this.itemTo = 0;
        }
        else {
          this.itemFrom = (this.pageNumber - 1) * this.pageSize + 1;
          this.itemTo = this.itemFrom + res.ListData.length - 1;
        }
      }, (err) => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-not-fount', null)));
  }

  public showAddModal(): void {
    this.resetForm();
    this.modalAdd.config = { backdrop: 'static' };
    this.listItem = new User();
    this.modalAdd.show();
  }

  public saveAdd(): void {
    this.submitted = true;
    this.listItem = this.formUser.value;
    this.listItem.Notification = true;
    this.listFacility = this.formUser.value.Selected;
    let param = {
      User: this.listItem,
      UPSFacilityIds: this.listFacility
    }
    if (!this.formUser.valid == true || this.listItem.GroupUserId == null || this.listFacility == null) {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-required', null));
    } else {
      this.checkRegexp();
      if (this.regexpUser == true) {
        this.userService.postCreateUser(JSON.stringify(param))
          .subscribe((response: any) => {
            this.modalAdd.hide();
            this.getAll();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-create-ok', null));
          }, (err: any) => {
            if (err._body.indexOf('UserADID') != -1 && err._body.indexOf('1111') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-ADID-check-exist', null));
            } else {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-create-err', null));
            }
          })
      } else {
        this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-check-value', null));
      }
    }
  }

  public showEditModal(id: string): void {
    this.loadUserDetail(id);
    this.UserId = id;
    this.modalEdit.show();
  }

  public loadUserDetail(id: string): void {
    this.userService.getDetailUser(id)
      .subscribe((response: any) => {
        this.formUser.patchValue({
          ADID: response.User.ADID,
          FullName: response.User.FullName,
          EmailAddress: response.User.EmailAddress,
          GroupUserId: response.User.GroupUserId,
          Selected: response.UPSFacilityIds,
        });
      });
  }

  public saveEdit(): void {
    this.submitted = true;
    this.listItem = this.formUser.value;
    this.listItem.UserId = this.UserId;
    this.listFacility = this.formUser.value.Selected;
    let param = {
      User: this.listItem,
      UPSFacilityIds: this.listFacility
    }
    if (!this.formUser.valid == true || this.listItem.GroupUserId == null || this.listFacility == null) {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-required', null));
    } else {
      this.checkRegexp();
      if (this.regexpUser == true) {
        this.userService.putUpdateUser(JSON.stringify(param))
          .subscribe((response: any) => {
            this.modalEdit.hide();
            this.getAll();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-update-ok', null));
          }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-update-err', null)));
      } else {
        this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-check-value', null));
      }
    }
  }

  public statusUser(id: string, status: boolean): void {
      if (status == true) {
        let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-user-body-deactivate', null), this.pipeTranslate.transform('msg-user-title-deactivate', null));
        mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
          this.userService.putUpdateStatusUser(id)
            .subscribe((res: any) => {
              this.getAll();
              this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-deactivate-ok', null));
            }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-deactivate-err', null)));
        }, (err: any) => {
          this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-mess-not-fount', null));
        })
      }
      else {
        let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-user-body-activate', null), this.pipeTranslate.transform('msg-user-title-activate', null));
        mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
          this.userService.putUpdateStatusUser(id)
            .subscribe((res: any) => {
              this.getAll();
              this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-activate-ok', null));
            }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-activate-err', null)));
        }), (err: any) => {
          this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-mess-not-fount', null));
        }
      }
  }

  public deleteItem(id: string): void {
    let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-user-body-delete', null), this.pipeTranslate.transform('msg-user-title-delete', null));
    mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
      this.userService.deleteUser(id)
        .subscribe((res: any) => {
          this.getAll();
          this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-delete-ok', null));
        }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-delete-ok', null)));
    }, (err: any) => {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-mess-not-fount', null));
    });
  }

  public selectAll(value: any): void {
    this.Users.forEach((user: User, index: number) => {
      user.IsSelected = value;
    });
  }

  public deleteMultiple(): void {
    this.userSelect = this.Users.filter((c: User, index: number) => { return c.IsSelected; })
      .map((p: User, index: number) => { return p.UserId; });
    if (this.userSelect.length > 0) {
      let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-user-body-delete', null), this.pipeTranslate.transform('msg-user-title-delete', null));
      mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
        this.userService.deleteMultiUser(this.userSelect)
          .subscribe((res: any) => {
            this.getAll();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-user-delete-ok', null));
          }, error => this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-user-delete-ok', null)));
      }, (err: any) => {
        this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-mess-not-fount', null));
      })
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-not-data', null))
    }
  }

  public sortItem(colName: string): void {
    if (this.orderBy == colName) {
      this.orderType = !this.orderType;
    } else {
      this.orderBy = colName;
      this.orderType = false;
    }
    this.getAll();
  }

  public pageChanged($event: any): void {
    this.pageNumber = $event.page;
    this.getAll();
  }

  public pageChangeSize(event : any): void {
    this.pageNumber = 1;
    this.getAll();
  }

  public resetSearch(): void {
    this.searchUser = this.resetSearchUser;
    this.fromInputValue(false);
    this.toInputValue(false);
    this.pageNumber = 1;
    this.getAll();
  }

  public searchItem(): void {
    this.conditionUser = this.formUserSearch.value;
    this.conditionUser.From = new DatePipe('en-US').transform(this.conditionUser.From, 'yyyy/MM/dd HH:mm:ss');
    this.conditionUser.To = new DatePipe('en-US').transform(this.conditionUser.To, 'yyyy/MM/dd HH:mm:ss');
    if (this.conditionUser.From == null || this.conditionUser.To == null || this.conditionUser.From <= this.conditionUser.To) {
      this.searchUser = this.conditionUser;
      this.pageNumber = 1;
      this.getAll();
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-wrong-format', null));
    }
  }

  public checkRegexp() {
    if (
      this.listItem.EmailAddress.match(CommonRegexp.EMAIL_ADDRESS_REGEXP)) {
      this.regexpUser = true;
    } else {
      this.regexpUser = false;
    }
    return this.regexpUser;
  }
}
