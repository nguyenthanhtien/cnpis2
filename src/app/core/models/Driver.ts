import { DriverCenterMasterData } from './DriverCenterMasterData';
import { KnownShipperModel } from 'src/app/core/models'; 
 
export class Driver {

  public DriverId: string;
  public DriverUPSEmpId: string;
  public DriverNationIdType?: string;
  public DriverNationId: string;
  public DriverName: string;
  public DriverPhoneNumber: string;

  public UPSFacilityId: string;  
  public UPSFacilityName: string;
  public UPSFacilitySLIC?: string;
  public Street?: string; 

  public PostalCode?: string;
  public DriverType?: string;
  public DriverCompany?: string;
  public DriverStatus: number;

  public CreatedBy?: string;
  public CreatedTime: Date;
  public UpdatedBy?: string;
  public UpdatedTime: Date;

  public DeviceId: string;
  public Remarks?: string; 

  public UPSFacility: DriverCenterMasterData ;  
}
export class DriverQueryResult {
  public ListData: Driver[];
  public TotalItems: number;
}
export class SearchDriver {
  public UPSEmpId: string;
  public Name: string;
  public Type: number;
  public NationId: string;
  public Status: number;
  public UPSFacility: string;  
  public SLIC: string;  
  public PhoneNumber: number;  
  public Company: string;  
}


