import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { mainRoutes } from './main.routes';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MessageBoxComponent, DataService, UtilityService, NotificationService } from 'src/app/core/services';
import { HTTP_INTERCEPTORS } from '../../../../node_modules/@angular/common/http';
import { ReponseInterceptor } from '../../core/interceptor/http.interceptor';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(mainRoutes),
    FormsModule,
  ],
  declarations: [
    MessageBoxComponent,
    
  ],
  entryComponents: [MessageBoxComponent],
  providers: [
    DataService,
    UtilityService,
    NotificationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ReponseInterceptor,
      multi: true,
  }
  ]
})
export class MainModule { }
