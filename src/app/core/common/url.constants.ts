export class UrlConstants {
  public static LOGIN = '/login';
  public static MAIN = '/main/';
  public static DRIVER_MANAGER = '/main/driver';
  public static PICKUPSHIPMENT_MANAGER = '/main/pickupshipment';
  public static USER_MANAGER = '/main/user';
  public static UPLOAD_SHIPMENT = '/main/pickupshipment';

  // URL-API-USER
  public static USER_SEARCH = '/api/Users/SearchUsers';
  public static USER_CREATE = '/api/Users';
  public static USER_UPDATE = '/api/Users/';
  public static USER_DETAIL = '/api/Users/GetUserById?id=';
  public static USER_DELETE = '/api/Users/';
  public static USER_DELETEMULTI = '/api/Users/MultiDelete/';
  public static USER_UPDATESTATUS = '/api/Users/UpdateStatus?id=';

  public static GROUPUSER_GETALL = '/api/GroupUsers';
  public static FACILITY_GETALL = '/api/UPSFacilityMasterData/';

  // URL-API-ICKUPSHIPMENT
  public static PICKUPSHIPMENT_SEARCH = '/api/PickupShipments/SearchPickupShipment';
  public static PICKUPSHIPMENT_UPDATE = '/api/PickupShipments';
  public static PICKUPSHIPMENT_DETAIL = '/api/PickupShipments/';
  public static PICKUPSHIPMENT_DELETE = '/api/PickupShipments/';
  public static PICKUPSHIPMENT_DELETEMULTI = '/api/PickupShipments/MultiDelete/';
  public static PICKUPSHIPMENT_VALIDATION = "/api/PickupShipments/CheckPickupShipmentValidation/";
  public static PICKUPSHIPMENT_EXPORTREPORT = "/api/PickupShipments/ReportPickupShipment/";

  // URL-API-DRIVER
  public static API_POST_DRIVER = '/api/Drivers';
  public static API_PUT_DRIVER = '/api/Drivers';
  public static API_PUT_STATUS = '/api/Drivers/';
  public static API_GET_DRIVER = '/api/Drivers/SearchDrivers';
  public static API_GET_ALL_DRIVER = '/api/Drivers/GetAllDriverForShipment';
  public static API_GET_DRIVER_DETAIL = '/api/Drivers/';
  public static API_GETALL_KNOWNSHIPPER = '/api/KnownShipperMasterData';
  public static API_GETALL_UPSFACILITYMASTERDATA = '/api/UPSFacilityMasterData';
  public static API_GET_UPSFACILITYMASTERDATA_BY_ID = '/api/UPSFacilityMasterData/';
  // Url KNOW SHIPPER
  public static GET_SHIPPER = '/api/KnownShipperMasterData/SearchKnownShippers';
  public static MODIFY_SHIPPER = '/api/KnownShipperMasterData';
  public static GETID_SHIPPER = '/api/KnownShipperMasterData/';
  public static MULTIPLE_DELETE_SHIPPER = '/api/KnownShipperMasterData/MultiDelete';

  // Url Province
  public static PROVINCE_SEARCH = '/api/ProvincesMasterData/SearchProvinces';
  public static PROVINCE_CREATE = '/api/ProvincesMasterData';
  public static PROVINCE_UPDATE = '/api/ProvincesMasterData';
  public static PROVINCE_DETAIL = '/api/ProvincesMasterData/';
  public static PROVINCE_DELETE = '/api/ProvincesMasterData/';
  public static PROVINCE_DELETEMULTI = '/api/ProvincesMasterData/MultiDelete/';

  // URL County
  public static API_COUNTY_SEARCH = '/api/CountiesMasterData/SearchCounties';
  public static API_COUNTY_CREATE = '/api/CountiesMasterData/';
  public static API_COUNTY_UPDATE = '/api/CountiesMasterData/';
  public static API_COUNTY_DETAIL = '/api/CountiesMasterData/';
  public static API_COUNTY_DELETE = '/api/CountiesMasterData/';
  public static API_COUNTY_DELETEMULTI = '/api/CountiesMasterData/MultiDelete/';
  public static API_COUNTY_CITY_GETALL = '/api/CitiesMasterData';

  // URL driverCenter
  public static DRIVER_CENTERS = '/api/UPSFacilityMasterData';
  public static DRIVER_CENTERS_SEARCH = '/api/UPSFacility/SearchUPSFacility';
  public static DRIVER_CENTERS_CREATE = '/api/UPSFacilityMasterData';
  public static DRIVER_CENTERS_UPDATE = '/api/UPSFacilityMasterData';
  public static DRIVER_CENTERS_DETAIL = '/api/UPSFacilityMasterData/';
  public static DRIVER_CENTERS_DELETE = '/api/UPSFacilityMasterData/';
  public static DRIVER_CENTERS_DELETEMULTI = '/api/UPSFacility/MultiDelete';
  public static GET_ALL_COUNTY = '/api/CountiesMasterData';
  public static GET_ALL_PROVINCE = '/api/ProvincesMasterData';
  public static GET_ALL_COUNTY_BY_CITY = '/api/CountiesMasterData/GetListCountybyCity?id=';
  public static GET_ALL_DRIVER_LIST= '/api/Drivers/GetAllDriverForShipment';
  public static GET_ALL_DRIVER_LIST_BY_ID= '/api/Drivers/';

  public static CITY_GETALL= '/api/CitiesMasterData/';
  // URL City
  public static API_CITY_SEARCH = '/api/CitiesMasterData/SearchCities';
  public static API_CITY_CREATE = '/api/CitiesMasterData/';
  public static API_CITY_UPDATE = '/api/CitiesMasterData/';
  public static API_CITY_DETAIL = '/api/CitiesMasterData/';
  public static API_CITY_DELETE = '/api/CitiesMasterData/';
  public static API_CITY_GET_PROVINCE = '/api/ProvincesMasterData';
  public static API_CITY_DELETEMULTI = '/api/CitiesMasterData/MultiDelete/';
  public static API_CITY_IN_PROVINCE = '/api/CitiesMasterData/GetCitiesByProvinceId?id=';

  // URL Upload
  public static UPLOAD_DRIVER_PROFILE_FILE = '/api/Drivers/UploadFile';
  public static POST_DATA_PROFILE = '/api/PickupShipments/ImportPickupShipments';
  public static UPLOAD_PICKUP_SHIPMENT_FILE = '/api/PickupShipments/UploadPickupShipment';
  public static DOWNLOAD_TEMPLATE_PROFILE = '/api/Drivers/DownloadTemplate';
  public static DOWNLOAD_TEMPLATE_PICKUPSHIPMENT = '/api/PickupShipments/DownloadTemplate';
  public static FILENAME_PROFILE_TEMPLATE = 'Driver Profile Template.xlsx';
  public static FILENAME_PICKUPSHIPMENT_TEMPLATE = 'Pickup Shipment Upload Template.xlsx';
  public static FILENAME_PICKUPSHIPMENT_REPORT = '_Report.xlsx';
}
