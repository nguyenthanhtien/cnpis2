export class QueryPickupShipment
{
    public TrackingNo: string;
    public DriverId: string;
    public PackageType: string;
    public ShipperId: string;
    public From: Date;
    public To: Date;
  
}