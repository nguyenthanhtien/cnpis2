import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'facility',
  pure: false 
})

export class FacilityPipe implements PipeTransform {

  constructor() { }

  transform(value: any[]): string {
    let facilities: string = '';
    value.forEach((e, index: number) => {
        facilities = facilities.concat((facilities != ''?', ':'') + e.UPSFacilitySLIC);
    });
    return facilities;
  }
}