import { Time } from "ngx-bootstrap/timepicker/timepicker.models";
import { City } from "./City";
import { GroupUser } from "./GroupUser";
import { DriverCenterMasterData } from ".";

export class User {

    public UserId: string

    public ADID: string

    public FullName: string

    public EmailAddress: string

    public Notification: boolean

    public GroupUserId: string

    public GroupUser : GroupUser

    public CreateTime: Time

    public LastLoggedIn: Time

    public Status: boolean    

    public Deleted: boolean     

    public IsSelected: boolean

    public From: string

    public To: string

    public GroupName: string

    public Facility: Facility[];

    public UPSFacilityIds : Array<FacilityList>

    public DriverCenterMasterData: DriverCenterMasterData

    constructor() {
        this.FullName = null;
        this.EmailAddress = null;
        this.ADID = null;
        this.Status = null
        this.GroupUserId= '';
        this.CreateTime = null;
        this.Facility = null;
        this.From = null;
        this.To = null;
    }
}
export class Facility {

    public UPSFacilityId: string;
    public UPSFacilitySLIC: string;

    constructor() {
        this.UPSFacilityId = '';
        this.UPSFacilitySLIC = '';
    }
}

export class FacilityList {

    public UPSFacilityIds: string;

    constructor() {
        this.UPSFacilityIds = '';
    }
}
export class UserQueryResult {
    public ListData: User[];
    public TotalItems: number;
  }