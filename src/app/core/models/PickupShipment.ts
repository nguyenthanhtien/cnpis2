import { Driver } from "./Driver";

export class PickupShipment{
    public ShipmentId: string

    public TrackingNo: string

    public PickUpDateTime: Date

    public PackageType: string

    public ShipperUPSAccNum: string
    
    public DriverNationalId: string

    public PickUpAddress: string

    public DriverUPSEmpId: string;

    public PackageQuantity: number

    public ConsigneePhoneNumber: string

    public Deleted: boolean

    public IsSelected: boolean

    public From: string
    
    public To: string 

    public DriverId: string

    public Driver: Driver

    constructor(){
        this.TrackingNo= null;
        this.DriverId = null;
        this.DriverNationalId = null;
        this.DriverUPSEmpId = null;
        this.ShipperUPSAccNum = null;
        this.From = null;
        this.To = null;
        this.PackageType = "";
    }
}
export class PickupShipmentQueryResult {
    public ListData: PickupShipment[];
    public TotalItems: number;
  }