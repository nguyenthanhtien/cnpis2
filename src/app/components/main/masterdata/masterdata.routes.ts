import { Routes } from '@angular/router';
import { ProvinceComponent } from './province/province.component';
import { CityComponent } from './city/city.component';
import { CountyListComponent } from './county/county-list/county-list.component';
import { DrivercenterComponent } from './drivercenter/drivercenter.component';
import { OrganizationComponent } from './organization/organization.component';
import { AUTHGROUP } from 'src/app/core/common';
import { RoleGuardService } from 'src/app/core/services';

export const mainRoutes: Routes = [

    { path: 'province', component: ProvinceComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'city', component: CityComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'driver-center', component: DrivercenterComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'organization', component: OrganizationComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } },

    { path: 'county', component: CountyListComponent, canActivate: [RoleGuardService], data: { expectedRole: [AUTHGROUP.SYSTEM_ADMIN] } }
]