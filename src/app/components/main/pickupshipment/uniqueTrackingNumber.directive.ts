import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

import { PickupShipmentUpload } from '../../../core/models/PickupShipmentUpload';

export function uniqueTrackingNumberValidator(editShipment: PickupShipmentUpload, shipments: PickupShipmentUpload[]): ValidatorFn {
    return (control: AbstractControl) : {[key: string]: any} | null => {
        if (shipments == null) {
            return null;
        }
        let found: boolean = false;
        shipments.forEach((shipment: PickupShipmentUpload, index: number) => {
            if (shipment.Index != editShipment.Index) {
                if (control.value == shipment.TrackingNo) {
                    found = true;
                }
            }
        });
        return found ? { duplicate: found } : null;
    };
}

@Directive({
    selector: '[uniqueTrackingNumber]',
    providers: [{ provide: NG_VALIDATORS, useExisting: uniqueTrackingNumberDirective, multi: true }]
})

export class uniqueTrackingNumberDirective implements Validator {
    @Input() editShipment: PickupShipmentUpload;
    @Input() shipments: PickupShipmentUpload[];

    validate(c: AbstractControl): {[key: string]: any} | null {
        return uniqueTrackingNumberValidator(this.editShipment, this.shipments)(c);
    }

    registerOnValidatorChange?(fn: () => void): void {
    }
}