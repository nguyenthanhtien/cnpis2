import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EdimessagescheduleComponent } from './edimessageschedule/edimessageschedule.component';
import { IpaddressmanagementComponent } from './ipaddressmanagement/ipaddressmanagement.component';
import { IntergrationconfigurationComponent } from './intergrationconfiguration/intergrationconfiguration.component';
import { ArchiveconfigurationComponent } from './archiveconfiguration/archiveconfiguration.component';
import { NotificationconfigurationComponent } from './notificationconfiguration/notificationconfiguration.component';
import { mainRoutes } from './configurationsetting.routes';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '../../../core/pipes/translate.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(mainRoutes),
    TranslateModule,
    ReactiveFormsModule
    
  ],
  declarations: [
    EdimessagescheduleComponent,
    IpaddressmanagementComponent,
    IntergrationconfigurationComponent,
    ArchiveconfigurationComponent,
    NotificationconfigurationComponent,   
  ]
})
export class ConfigurationsettingModule { }
