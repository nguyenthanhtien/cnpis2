import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { appRoutes } from './app.routes';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HeaderComponent } from './shared/header/header.component'
import { LeftmenuComponent } from './shared/leftmenu/leftmenu.component'

import { AppComponent } from './app.component';
import { LoginModule } from 'src/app/components/login/login.module';
import { TranslateService, AuthGuardService, AuthService, UploadService, UtilityService, RoleGuardService } from 'src/app/core/services';
import { PageNotFoundComponent } from './shared/404/pagenotfound.component';
import { TranslateModule } from './core/pipes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftmenuComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    LoginModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    TranslateModule
  ],
  providers: [
    TranslateService,
    AuthGuardService,
    AuthService,
    UploadService,
    UtilityService,
    RoleGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
