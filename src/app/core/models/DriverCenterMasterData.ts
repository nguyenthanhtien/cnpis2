import { County } from "./County";
import { ProvinceMasterData } from "./ProvinceMasterData";
import { City } from "./City";
import { Driver } from "./Driver";


export class DriverCenterMasterData {
   public IsSelected :boolean;
   public UPSFacilityId: string;
   public UPSFacilitySLIC: string;
   public UPSFacilityName: string;
   public PostalCode: number;
   public Street : string;
   public CountyId : string;
   public County : County;
   public ProvinceId: string;
   public Province: ProvinceMasterData;
   public City: City;
   public CityId : string;
   public Driver: Driver;
   public DriverId:string;
   public DriverName: string;
   public DriverUPSEmpId: string;
   constructor(){
       this.IsSelected = null;
       this.UPSFacilityId = null;
       this.UPSFacilitySLIC = null;
       this.UPSFacilityName = null;
       this.PostalCode = null;
       this.Street = null;
       this.CountyId = null;
       this.CityId= null
       this.ProvinceId = null;
       this.County = new County() ;
       this.Province = null;
       this.City =  new City();
       this.Driver = new Driver();
       this.DriverId = null;

   }
}
export class DriverCentersQueryResult {
    public ListData: DriverCenterMasterData[];
    public TotalItems: number;
  }