import { City } from './City';
import { ProvinceMasterData } from './ProvinceMasterData';

export class County {

  public IsSelected: boolean;

  public CountyId: string;

  public CountyCode: string;

  public CountyName: string;

  public CityId: string;

  public ProvinceId: string;

  public ProvinceCode: string;

  public ProvinceName: string;

  public CityName: string;

  public CityCode: string;

  public City: City;

  public Province: ProvinceMasterData;

}
