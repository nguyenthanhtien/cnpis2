export class KnownShipperModel {
    public KnownShipperId : string

    public ShipperAccount : string

    public KnownShipperName : string

    public PostalCode : string
    
    public KnownShipperTaxNo : string

    public PhoneNumber : string

    public Street : string

    public ContactPerson : string

    public selected: boolean

}