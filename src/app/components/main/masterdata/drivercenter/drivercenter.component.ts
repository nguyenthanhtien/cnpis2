import { Component, OnInit, ViewChild, Output, Input,EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DriverCenterMasterData, DriverCentersQueryResult } from 'src/app/core/models/DriverCenterMasterData';
import { DataService, NotificationService, MessageBox } from 'src/app/core/services';
import { DriverCenterService, ProvinceService, CityService, CountyService } from '../../../../core/api/masterdata.service';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { City, County, ProvinceMasterData } from '../../../../core/models/';
import { DriverCenterAdd } from '../../../../core/models/DriverCenterModelAdd';
import { Driver } from 'src/app/core/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
@Component({
  selector: 'app-drivercenter',
  templateUrl: './drivercenter.component.html',
  providers: [TranslatePipe, DataService, MessageBox]
})
export class DrivercenterComponent implements OnInit {

  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;
  @ViewChild('modalEdit') public modalEdit: ModalDirective;
  @Input() County: County;
  @Output() OnSaveChanged: EventEmitter<DriverCenterMasterData> = new EventEmitter<DriverCenterMasterData>();

  public listDriverMaster: Observable<DriverCenterMasterData[]>;
  public driverCenter: DriverCenterMasterData;
  public formatDate;
  public pageNumber: number;
  public orderBy: string;
  public orderType: boolean;
  public pageSize: number;

  public searchSLIC: string;
  public searchName: string;
  public searchCode: string;
  public searchProvince: string = null;
  public searchCity: string = null;
  public searchCounty: string = null;

  public totalItems: number;
  public DriverCenters: DriverCenterMasterData[] = [];
  public selectedIDs: any[] = [];
  public noResultFound: boolean = false;
  public sortColumn: string = 'UPSFacilityName';
  public isSorted: boolean = true;
  public Cities: City[] = [];
  public Countys: County[] = [];
  public Provinces: ProvinceMasterData[] = [];
  public Drivers: Driver[] = [];
  public registerForm: FormGroup;
  public driverCenterAdd: DriverCenterAdd;
  public submitted: boolean;
  
  constructor(public dataService: DataService,
    public notificationService: NotificationService,
    public driverCenterService: DriverCenterService,
    public msgBox: MessageBox,
    private pipeTranslate: TranslatePipe,
    private provinceService: ProvinceService,
    private cityService: CityService,
    private countyService: CountyService,
    private formBuilder: FormBuilder
  ) {
    this.driverCenterAdd = new DriverCenterAdd();
  }

  ngOnInit() {
    console.log(this.DriverCenters);
    this.pageSize = 20;
    this.pageNumber = 1;
    this.getAll(); 
    this.createForm();
  }
  resetForm() {
    this.searchSLIC = null;
    this.searchName = null;
    this.searchCode = null;
    this.searchProvince = null;
    this.searchCity = null;
    this.searchCounty = null;
    this.getAll();
    this.createForm();
  }
  createForm(): void {
    this.registerForm = this.formBuilder.group({
      UPSFacilityId: [""],
      UPSFacilitySLIC: ["", [Validators.required, Validators.maxLength(16)]],
      UPSFacilityName: ["", [Validators.required, Validators.maxLength(64)]],
      PostalCode: ["", [Validators.required, Validators.pattern('[0-9]+'), Validators.maxLength(255)]],
      ProvinceId: [""],
      ProvinceName: ["", [Validators.required, Validators.maxLength(255)]],
      CityId: [""],
      CityName: ["", [Validators.required, Validators.maxLength(30)]],
      CountyId: [""],
      CountyName: ["", Validators.required],
      Street: ["", [Validators.required, Validators.maxLength(255)]],
      DriverId: [""],
      DefaultDriver: [""],
      DriverName: [""]
    });
  }
  get cityFrm() {
    return this.registerForm.controls;
  }
  public getAllDriverCenterById(id: any) {
    this.driverCenterService.getDetailDriverCenter(id).subscribe((response: any) => {
      this.cityFrm.UPSFacilityId.setValue(response.UPSFacilityId);
      this.cityFrm.UPSFacilityName.setValue(response.UPSFacilityName);
      this.cityFrm.UPSFacilitySLIC.setValue(response.UPSFacilitySLIC);
      this.cityFrm.PostalCode.setValue(response.PostalCode);
      this.provinceService
        .getDetailProvince(response.County.ProvinceId)
        .subscribe((res: any) => {
          this.cityFrm.ProvinceName.setValue(res.ProvinceName);
          this.cityFrm.ProvinceId.setValue(res.ProvinceId);
        },);
      this.cityService.getCityById(response.County.CityId).subscribe(
        (res: any) => {
          this.cityFrm.CityName.setValue(res.CityName);
          this.cityFrm.CityId.setValue(res.CityId);
        }
      );
      this.countyService.getCountyId(response.CountyId).subscribe(
        (res: any) => {
          this.cityFrm.CountyName.setValue(res.CountyName);
          this.cityFrm.CountyId.setValue(res.CountyId);
        }
      );
      this.cityFrm.Street.setValue(response.Street);
      this.cityFrm.DriverId.setValue(response.DriverId);
      this.cityFrm.DefaultDriver.setValue(response.DefaultDriver);
      this.cityFrm.DriverName.setValue(response.DriverName);
    }, );
    
  }
  private sort(columnName: any): void {
    if (this.sortColumn == columnName) {
      this.isSorted = !this.isSorted;
    } else {
      this.sortColumn = columnName;
      this.isSorted = false;
    }
    this.getAll();
  }

  public pageChanged($event: any): void {
    this.pageNumber = $event.page;
    this.getAll();
  }
  public getCities(): void {
    this.driverCenterService.getAllCity().subscribe((response: any) => {
      this.Cities = response;
    });
  }

  public getCountys(): void {
    this.driverCenterService.getAllCouty().subscribe((response: any) => {
      this.Countys = response;
    });
  }

  public getProvinces(): void {
    this.driverCenterService.getAllProvince().subscribe((response: any) => {
      this.Provinces = response;
    });
  }
  public getDriverList(): void {
    this.driverCenterService.getAllDriverList().subscribe((response: any) => {
      this.Drivers = response;
    });
  }

  public onItemChangeProvinceCode($event) {
    if (_.includes(this.Provinces, $event.item)) {
      this.cityFrm.ProvinceId.setValue($event.item.ProvinceId);
      this.cityFrm.CityName.setValue('');
      this.cityService.getAllCityInProvince($event.item.ProvinceId).subscribe((res: any) => {
        this.Cities = res;
      });
    } else {
      this.cityFrm.ProvinceName.setValue('');
    }
  }
  
  public onChangeDriverCenterCode($event) {
    if (_.includes(this.Drivers, $event.item)) {
      this.cityFrm.DriverName.setValue($event.item.DriverName);
      this.cityFrm.DriverId.setValue($event.item.DriverId);
    } else {
      this.cityFrm.DefaultDriver.setValue('');
    }
  }
  public onItemChangeCityCode($event) {
    if (_.includes(this.Cities, $event.item)) {
      this.cityFrm.CityId.setValue($event.item.CityId);
      this.cityFrm.CountyName.setValue('');
      this.driverCenterService.getAllCountyByCity($event.item.CityId).subscribe((res: any) => {
        this.Countys = res;
      });
    } else {
      this.cityFrm.CityName.setValue('');
    }
  }
  public onItemChangeCountyCode($event) {
    if (_.includes(this.Countys, $event.item)) {
      this.cityFrm.CountyId.setValue($event.item.CountyId);
    } else {
      this.cityFrm.CountyName.setValue('');
    }
  }

  public getAll() {
    let params = {
      'PageSize': this.pageSize, 'PageNumber': this.pageNumber,
      'OrderBy': this.sortColumn, 'OrderType': this.isSorted,
      'UPSFacilitySLIC': this.searchSLIC,
      'UPSFacilityName': this.searchName,
      'PostalCode': this.searchCode,
    };
    this.driverCenterService.getAllDriverCenter(params)
      .subscribe(
        (queryResult: DriverCentersQueryResult) => {
          this.totalItems = queryResult.TotalItems;
          if (this.totalItems > 0) {
            this.noResultFound = false;
            this.DriverCenters = queryResult.ListData;
          } else {
            this.noResultFound = true;
          }
        }, (err: any) => { });
  }
  public selectAll(value: any): void {
    this.DriverCenters.forEach((DriverCenterMasterData: DriverCenterMasterData, index: number) => {
      DriverCenterMasterData.IsSelected = value;
    });
  }

  public saveDriverCenter() {
    this.submitted = true;
    if (this.registerForm.valid === true) {
      const obj = this.registerForm.getRawValue();
      if (obj.UPSFacilityId === '') {
        this.driverCenterService.postCreateDriverCenter(obj)
          .subscribe((res: any) => {
            this.OnSaveChanged.emit(res);
            this.modalAddEdit.hide();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-add-ok', null));
            this.getAll();
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-ups-connection', null))
            }
            if (err._body.indexOf('UPSFacilitySLIC, 1111') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-slic-check-exist', null))
            }
          });
      } else {
        let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-user-body-update', null), this.pipeTranslate.transform('msg-user-title-update', null));
        mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
          this.driverCenterService.putUpdateDriverCenter(obj)
            .subscribe((response: any) => {
              this.modalAddEdit.hide();
              this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-update-ok', null));
              this.getAll();
            }, (err: any) => {
              if (err.status === 0) {
                this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-edit-ups-connection', null))
              }
              if (err._body.indexOf('UPSFacilitySLIC, 1111') != -1) {
                this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-slic-check-exist', null))
              }
            });
        }, (err: any) => {
          if (err._body.indexOf('DriverCenterSLIC, 1111') != -1) {
            this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-slic-check-exist', null))
          }
        });
      }
    }
  }
  showAddModal(): void {
    this.driverCenter = new DriverCenterMasterData();
    this.getProvinces();
    this.getCities();
    this.getCountys();
    this.getDriverList();
    this.modalAddEdit.show();
  }
  isEdited: any = false;
  showEditModal(id: any) {
    this.isEdited = true;
    this.getProvinces();
    this.getCities();
    this.getCountys();
    this.getDriverList();
    this.getAllDriverCenterById(id);
    this.modalAddEdit.show();
  }

  public loadUserDetail(id: any) {
    this.driverCenterService.getDetailDriverCenter(id)
      .subscribe((response: DriverCenterMasterData) => {
        this.driverCenter = response;
        console.log(this.driverCenter);
      });
  }

  public deleteDriverCenter(id: string): void {
    let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-driver-body-delete', null));
    mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
      if (id != null) {
        this.driverCenterService.deleteDriverCenter(id)
          .subscribe((res: any) => {
            this.getAll();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-delete-ok', null));
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-ups-connection', null))
            }
            if (err._body.indexOf('DRIVERID, 113') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-can-not-delete', null));
            }
          });
      }
    }, (err: any) => { });
  }
  public deleteMultiDataDriverCenter(): void {
    this.selectedIDs = this.DriverCenters.filter((drivercenter: DriverCenterMasterData, index: number) => { return drivercenter.IsSelected; })
      .map((p: DriverCenterMasterData, index: number) => { return p.UPSFacilityId; });
    if (this.selectedIDs != null && this.selectedIDs.length > 0) {
      let mdlRef = this.msgBox.confirm(this.pipeTranslate.transform('msg-driver-body-delete', null), this.pipeTranslate.transform('msg-province-title-delete', null));
      mdlRef.content.OnResult.subscribe((msgBoxResult: any) => {
        this.driverCenterService.deleteMultiDriverCenter(this.selectedIDs)
          .subscribe((res: any) => {
            this.getAll();
            this.notificationService.printSuccessMessage(this.pipeTranslate.transform('msg-province-delete-ok', null));
          }, (err: any) => {
            if (err.status === 0) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('Error-ups-connection', null))
            }
            if (err._body.indexOf('DRIVERID, 113') != -1) {
              this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-can-not-multidelete', null))
            }
          });
      }, (err: any) => {
        this.notificationService.printErrorMessage(err);
      });
    } else {
      this.notificationService.printErrorMessage(this.pipeTranslate.transform('msg-not-1-fount', null))
    }
  }
}
