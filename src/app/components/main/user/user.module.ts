import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, ModalModule, AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './user.routes';
import { BsDatepickerModule, TimepickerModule } from 'ngx-bootstrap';
import { TranslateModule } from 'src/app/core/pipes/translate.module';
import { DataService, NotificationService } from 'src/app/core/services';
import { UserService } from 'src/app/core/api';
import { DualListBoxModule } from 'ng2-dual-list-box';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forChild(mainRoutes),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    TranslateModule,
    ReactiveFormsModule,
    DualListBoxModule.forRoot(),
    AngularDateTimePickerModule
  ],
  exports: [
     
  ],
  declarations: [
    UserComponent, 
    ],
  entryComponents: [],
  providers: [
    DataService,
    NotificationService,
    UserService
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class UserModule { }
