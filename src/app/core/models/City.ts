import { ProvinceMasterData } from './ProvinceMasterData';


export class City {
    public IsSelected: boolean;

    public CityId: string;

    public CityCode: string;

    public CityName: string;

    public ProvinceId: string;

    public ProvinceName: string;

    public Province: ProvinceMasterData;

}
export class CityQueryResult {
    public ListData: City[];
    public TotalItems: number;
}
