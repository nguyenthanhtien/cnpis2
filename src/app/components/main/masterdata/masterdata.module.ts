import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule, ModalModule, AlertModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './masterdata.routes';
import { ProvinceComponent } from './province/province.component';
import { CityComponent } from './city/city.component';
import { DrivercenterComponent } from './drivercenter/drivercenter.component';
import { OrganizationComponent } from './organization/organization.component';
import { CountyListComponent } from './county/county-list/county-list.component';
import { CountyDetailComponent } from './county/county-detail/county-detail.component';
import { MessageBoxComponent, MessageBox } from '../../../core/services/modal.service';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';
import { KnownShipperService, CityService } from '../../../core/api/masterdata.service';
import { TranslateModule } from '../../../core/pipes';
import { ProvinceService, DriverCenterService } from '../../../core/api/masterdata.service';
import { CountyService } from '../../../core/api/masterdata.service';
import { TypeaheadModule } from 'ngx-bootstrap';
import { HTTP_INTERCEPTORS } from '../../../../../node_modules/@angular/common/http';
import { ReponseInterceptor } from '../../../core/interceptor/http.interceptor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    RouterModule.forChild(mainRoutes),
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProvinceComponent,
    CityComponent,
    DrivercenterComponent,
    OrganizationComponent,
    CountyListComponent,
    CountyDetailComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ReponseInterceptor,
      multi: true
    },
    KnownShipperService,
    ProvinceService,
    CountyService,
    CityService,
    DriverCenterService,
    MessageBox
  ],
  entryComponents: [
    CountyDetailComponent
  ]
})
export class MasterdataModule { }
