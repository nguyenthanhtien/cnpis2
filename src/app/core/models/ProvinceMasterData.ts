export class ProvinceMasterData {
  public IsSelected: boolean;
  public ProvinceId: string;
  public ProvinceName: string;
  public ProvinceCode: number;
  constructor() {
    this.IsSelected= null;
    this.ProvinceId = null;
    this.ProvinceName =  null;
    this.ProvinceCode =  null;
  }
}

export class ProvinceQueryResult {
  public ListData: ProvinceMasterData[];
  public TotalItems: number;
}

