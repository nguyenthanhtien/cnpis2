import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-notificationconfiguration',
  templateUrl: './notificationconfiguration.component.html',
})

export class NotificationconfigurationComponent implements OnInit {

  notificationForm: FormGroup;
  
  modalTemplate : INotificationModel;
  public categories : ICategoryModel[];
  constructor(private httpService : Http,
    private formBuilder : FormBuilder) {
   }
  ngOnInit() {
    this.initCategory();
    this.initFormNotification();
    this.modalTemplate = new INotificationModel();
  }
  cancelModal(){

  }
  initCategory(){
    this.httpService.get('http://5b72dc4d414e970014304af2.mockapi.io/category').subscribe( (res : any) => {
        this.categories = JSON.parse(res._body) ;
      console.log(res._body)} );
  }
  initFormNotification(){
    this.notificationForm = this.formBuilder.group({
      templateNameControl : ['',[Validators.maxLength(50)]],
      isActiveControl : ['',[Validators.maxLength(50)]],
      categoryIDControl : ['',[Validators.maxLength(50)]],
      sentToControl : ['',[Validators.maxLength(50)]],
      ccControl : ['',[Validators.maxLength(50)]],
      bccControl : ['',[Validators.maxLength(50)]],
      subjectControl : ['',[Validators.maxLength(50)]],
      messageBodyControl : ['',[Validators.maxLength(50)]],
    });
  }
  initNotificationTemplate(){

  }
  createNotificationTemplate(){

  }
  updateNotificationTemplate(){
    
  }
  deleteSingleTemplate(){

  }
  deleteMultiTemplate(){

  }
  get f() { return this.notificationForm.controls; }

  searchTemplate(){

  }
  onSelectedCategory(selectedValue){
    this.modalTemplate.categoryID = selectedValue;
  }
  resetSearch(){

  }
  

}
export class INotificationModel {
  public templateName : string;
  public isActive : boolean;
  public categoryID : string;
  public sentTo : string;
  public cc : string;
  public bcc : string;
  public subject : string;
  public messageBody : string;
}
export class INotificationSearchModel {
  public templateName : string;
  public isActive : boolean;
}
export class ICategoryModel {
  public categoryID : string;
  public categoryName : boolean;
}